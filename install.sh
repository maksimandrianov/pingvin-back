#!/bin/bash

apt-get update 
apt-get install --reinstall locales
apt-get install -y software-properties-common \
    apt-transport-https \
    ca-certificates \
    linux-image-extra-$(uname -r) \
    linux-image-extra-virtual \
    curl
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get update
apt-get install -y git \
    docker-ce
echo "DOCKER_OPTS=\"-s overlay\"" >> /etc/default/docker
service docker restart
curl -L https://github.com/docker/compose/releases/download/1.12.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose 
#git clone https://maksimandrianov@bitbucket.org/maksimandrianov/nproject.git

