#!/bin/sh

# wait db

until nc -z db 5432; do
    sleep 5
    echo "$(date) - waiting for postgres..."
done

cd nproject  

python manage.py makemigrations nmodels 
python manage.py migrate
python manage.py loaddata categories.json

gunicorn nproject.wsgi:application -w 4 -b 0.0.0.0:8000 & 

sleep 5
python manage.py start_sphinx

while true; do
    python manage.py index_sphinx
    echo "$(date) - sphinx reindex..."
    sleep 5m
done

