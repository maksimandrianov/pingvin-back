FROM python:3.5
ADD requirements.txt /app/requirements.txt
ADD scripts/sphinx_install.sh /app/sphinx_install.sh
WORKDIR /app/
RUN apt-get update && apt-get install -y \
    netcat \
    build-essential \
    libgeos++-dev \
    locales-all
RUN pip install -r requirements.txt
RUN ./sphinx_install.sh
RUN adduser --disabled-password --gecos '' user 

