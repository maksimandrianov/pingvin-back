from nmodels.models import User, Post, Help, UserSettings, CommentUser, CommentPost, Category
from django.db import connection
from nmodels.lib import dictfetchall
from django.db.models import Prefetch

class Qw:
    queryset_user_min = User.objects.all() \
        .only('first_name', 'second_name', 'avatar') \
        .select_related('avatar')

    @staticmethod
    def posts_list_optimization(q):
        return q.prefetch_related(Prefetch('created_user', queryset=Qw.queryset_user_min)) \
            .select_related('h_style') \
            .select_related('location') \
            .defer('updated', 'category_id', 'condition_id',
                   'location__city', 'location__country', 'location__address',
                   'views', 'is_blocked', 'media',
                   'req_helpers', 'moderated_time', 'organization')

    @staticmethod
    def events_list_optimization(q):
        return q.prefetch_related(Prefetch('created_user', queryset=Qw.queryset_user_min)) \
            .select_related('h_style') \
            .select_related('location') \
            .defer('updated', 'category_id', 'condition_id',
                   'location__city', 'location__country', 'location__address',
                   'views', 'is_blocked', 'media', 'moderated_time',)

    @staticmethod
    def query_help(post_id, limit=3):
        return '''SELECT "nmodels_help"."post_id", "nmodels_help"."user_id",
                "nmodels_user"."first_name", "nmodels_user"."second_name",
                "nmodels_mediaurl"."url", "nmodels_mediaurl"."type"
                FROM "nmodels_help"
                INNER JOIN "nmodels_user"
                ON ("nmodels_help"."user_id" = "nmodels_user"."id")
                LEFT OUTER JOIN "nmodels_mediaurl"
                ON ("nmodels_user"."avatar_id" = "nmodels_mediaurl"."id")
                WHERE ("nmodels_help"."is_head" = true AND "nmodels_help"."post_id" = %d)
                ORDER BY RANDOM() ASC LIMIT %d''' % (post_id, limit)


    @staticmethod
    def modify_posts_queryset(request, queryset):
        ids = [post.id for post in queryset]
        help_set = set(Post.get_group_helped(request.user.id, ids))

        like_objs = Post.get_group_liked(request.user.id, ids)
        like_dict = {like_obj['obj_id']: like_obj['is_like'] for like_obj in like_objs}

        helpers_dict = {}

        ids_uniq = list(set(ids))
        if len(ids_uniq) > 0:
            query_all_helpers = ''
            if len(ids_uniq) > 1:
                for id in ids_uniq[:len(ids_uniq)-1]:
                    query_all_helpers += '({}) UNION '.format(Qw.query_help(id))
                query_all_helpers += '({})'.format(Qw.query_help(ids_uniq[len(ids_uniq)-1]))
            elif len(ids_uniq) == 1:
                query_all_helpers = Qw.query_help(ids_uniq[0])

            cursor = connection.cursor()
            cursor.execute(query_all_helpers)

            for obj in dictfetchall(cursor):
                post_id = obj['post_id']
                del obj['post_id']
                if helpers_dict.get(post_id) is None:
                    helpers_dict[post_id] = [obj]
                else:
                    helpers_dict[post_id].append(obj)

        for post in queryset:
            post._is_help = post.id in help_set
            post._is_liked = like_dict.get(post.id)
            post._helpers = helpers_dict.get(post.id)

        return queryset