from rest_framework import status
from rest_framework.test import APITestCase
from nmodels.models import GeoInfo
from nmodels.serializers import SimplePostsSerializer
from nmodels import lib
from nmodels.tests.lib import Creator, Request
from django.contrib.gis.geos import Point


class BasicTestCasePostsSortWithGeoWithSettings(APITestCase):

    # Инженерная улица, 2-4Г (59.938244, 30.330626) (C)
    # набережная реки Мойки, 40 (59.937162, 30.320488) 590м
    # Большая Морская улица, 7 (59.936753, 30.317076)
    # Вознесенский проспект, 1 (59.934852, 30.307932)
    # Сенатская площадь, 3 (59.934911, 30.301761)
    # Английская набережная, 4 (59.935815, 30.299594)
    # Университетская набережная, 23 (59.936890, 30.287593)

    def setUp(self):
        location = GeoInfo.objects.create(country='Росиия',  city='Санкт-Петербург',
                                     address='Инженерная улица, 2-4Г', point= Point(59.938244, 30.330626))
        geos = [
            GeoInfo.objects.create(country='Росиия',  city='Санкт-Петербург',
                                   address='Университетская набережная, 23', point= Point(59.936890, 30.287593)),
            GeoInfo.objects.create(country='Росиия',  city='Санкт-Петербург',
                                   address='набережная реки Мойки, 40', point= Point(59.937162, 30.320488)),
            GeoInfo.objects.create(country='Росиия',  city='Санкт-Петербург',
                                   address='Большая Морская улица, 7', point= Point(59.936753, 30.317076)),
            GeoInfo.objects.create(country='Росиия',  city='Санкт-Петербург',
                                   address='Сенатская площадь, 3', point= Point(59.934911, 30.301761)),
            GeoInfo.objects.create(country='Росиия',  city='Санкт-Петербург',
                                   address='Вознесенский проспект, 1', point= Point(59.934852, 30.307932)),
            GeoInfo.objects.create(country='Росиия',  city='Санкт-Петербург',
                                   address='Английская набережная, 4', point= Point(59.935815, 30.299594)),
        ]

        self.user = Creator.create_user()
        self.user.settings.location = location
        self.user.settings.save()
        self.posts = Creator.create_posts_geo(geos, self.user)
        self.client.force_authenticate(user=self.user)

    def test_filed_simple_equal(self):
        request = Request(self.user, {})
        serializer = SimplePostsSerializer(self.posts[0], context={'request': request})
        data = serializer.data
        self.assertNotEqual(data['distance'], None)

    def test_posts_all_with_geo(self):
        url = lib.url('posts/')
        response = self.client.get(url)

        check_sort = True
        arr = response.data['results']
        for i in range(len(arr)-1):
            if arr[i]['distance'] > arr[i+1]['distance']:
                check_sort = False
                break

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(check_sort, True)

    def test_posts_with_geo_none(self):

        Creator.create_post(self.user)

        url = lib.url('posts/')
        response = self.client.get(url)

        check_sort = True
        arr = response.data['results']
        for i in range(len(arr)-2):
            if arr[i]['distance'] > arr[i+1]['distance']:
                check_sort = False
                break

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(check_sort, True)
        self.assertEqual(response.data['results'][-1]['distance'] is None, True)


class BasicTestCasePostsSortNonePostsWithSettings(APITestCase):

    def setUp(self):
        location = GeoInfo.objects.create(country='Росиия',  city='Санкт-Петербург',
                                     address='Инженерная улица, 2-4Г', point= Point(59.938244, 30.330626))

        self.user = Creator.create_user()
        self.user.settings.location = location
        self.user.settings.save()
        self.posts = Creator.create_posts(2, self.user, 0)
        self.client.force_authenticate(user=self.user)

    def test_filed_simple_equal(self):
        request = Request(self.user, {})
        serializer = SimplePostsSerializer(self.posts[0], context={'request': request})
        data = serializer.data
        self.assertEqual(data['distance'], None)

    # TODO RANDOM ERROR
    def test_posts_all_without_geo(self):
        url = lib.url('posts/')
        response = self.client.get(url)

        check_sort = True
        arr = response.data['results']

        for i in range(len(arr)):
            pass
            # if arr[i]['created'] < arr[i+1]['created']:
            #     check_sort = False
            #     break

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(check_sort, True)

