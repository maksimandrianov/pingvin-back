from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from nmodels.models import UserSettings, ConditionType, Help
from nmodels.serializers import UserSettingsSerializer
from nmodels import lib
from nmodels.tests.lib import Creator, Request
from django.contrib.gis.geos import Point


class BasicTestCaseSettingsPermission(APITestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.foreign_user = Creator.create_user(1)
        self.d_categories = [
            Creator.create_category_dict(1, 1),
            Creator.create_category_dict(2, 2),
            Creator.create_category_dict(3, 3)
        ]
        self.d_contacts = [
            Creator.create_contact_dict(1, '+79817068020'),
            Creator.create_contact_dict(2, 'test@test.ru'),
            Creator.create_contact_dict(3, 'https://vk.com/maxandr')
        ]

    def _req(self):
        url = lib.url('settings/' + str(self.user.id) + '/')
        self.user._update_categories(self.d_categories)
        self.user._update_contacts(self.d_contacts)
        data = {
            'contacts_ids': [i.id for i in self.user.contacts.all()],
            'categories_ids': [i.id for i in self.user.categories.all()],
            'location': {
                'country': 'Россия',
                'city': 'Санкт-Петербург',
                'address': 'Россия, Санкт-Петербург, пр. Энгельса 7',
                 'point':  {
                     'latitude': 1.1,
                     'longitude': 1.1
                 }
            }
        }
        return self.client.patch(url, data=data, format='json')

    def test_user_settings_update_anonymous(self):
        response = self._req()
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_settings_update_foreign(self):
        self.client.force_authenticate(user=self.foreign_user)
        response = self._req()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        url = lib.url('settings/' + str(self.user.id) + '/')
        _response = self.client.get(url, format='json')
        self.assertEqual(_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_settings_update_owner(self):
        self.client.force_authenticate(user=self.user)
        response = self._req()
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class NextPermission(APITestCase):

    def next_anonymous(self, url):
        response = self.client.get(url)
        url = response.data['next']
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def next_auth(self, url, user):
        self.client.force_authenticate(user=user)
        response = self.client.get(url)
        url = response.data['next']
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class BasicTestCaseUsersPermission(NextPermission):

    def setUp(self):
        Creator.create_users(2)
        self.user = Creator.create_user(100)

    def test_users_next_anonymous(self):
        url = lib.url('users/')
        self.next_anonymous(url)

    def test_users_next_auth(self):
        url = lib.url('users/')
        self.next_auth(url, self.user)


class BasicTestCasePostsPermission(NextPermission):

    def setUp(self):
        Creator.create_posts(2)
        self.user = Creator.create_user(100)

    def test_posts_next_anonymous(self):
        url = lib.url('posts/')
        self.next_anonymous(url)

    def test_posts_next_auth(self):
        url = lib.url('posts/')
        self.next_auth(url, self.user)


class BasicTestCasePostCommentPermission(NextPermission):

    def setUp(self):
        self.user = Creator.create_user()
        self.post = Creator.create_post(self.user)
        self.comment = Creator.create_comments_post(2, self.post, self.user)

    def test_post_comment_next_anonymous(self):
        url = lib.url('post-comments/' + str(self.post.id) + '/')
        self.next_anonymous(url)

    def test_posts_next_auth(self):
        url = lib.url('post-comments/' + str(self.post.id) + '/')
        self.next_auth(url, self.user)


class BasicTestCaseUserCommentPermission(NextPermission):

    def setUp(self):
        self.user = Creator.create_user()
        self.user = Creator.create_user(2)
        self.comment = Creator.create_comments_user(2, self.user, self.user)

    def test_user_comment_next_anonymous(self):
        url = lib.url('user-comments/' + str(self.user.id) + '/')
        self.next_anonymous(url)

    def test_user_next_auth(self):
        url = lib.url('user-comments/' + str(self.user.id) + '/')
        self.next_auth(url, self.user)


class BasicTestCaseCreateUserCommentPermission(APITestCase):

    def setUp(self):
        self.created_user = Creator.create_user()
        self.answered_him = Creator.create_user(1)
        self.user = Creator.create_user(2)
        self.post = Creator.create_post(self.user)
        self.client.force_authenticate(user=self.created_user)

    def test_user_comment_create_basic(self):

        condition = Creator.create_condition_dict(ConditionType.THANKS)
        self.created_user.help(self.post.id, condition)
        self.user.select_helper(self.post.id, self.created_user.id)
        self.user.select_successfully(self.post.id, self.created_user.id)

        url = lib.url('user-comments/' + str(self.user.id) + '/')
        data = {
            'text': 'Test text',
            'rating': 4,
            'answered_him': {'id': self.answered_him.id}
        }
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_comment_create_error(self):

        condition = Creator.create_condition_dict(ConditionType.THANKS)
        self.created_user.help(self.post.id, condition)
        self.user.select_helper(self.post.id, self.created_user.id)

        url = lib.url('user-comments/' + str(self.user.id) + '/')
        data = {
            'text': 'Test text',
            'rating': 4,
            'answered_him': {'id': self.answered_him.id}
        }
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class BasicTestCaseRepliesPermission(APITestCase):

    def setUp(self):
        self.user = Creator.create_user()

    def _replies(self):
        url = lib.url('replies/')
        response = self.client.get(url)
        return response.status_code, response.data

    def _replies_count(self):
        url = lib.url('replies/count/')
        response = self.client.get(url)
        return response.status_code, response.data

    def test_anonymous(self):
        code1, data1 = self._replies()
        code2, data2 = self._replies_count()
        self.assertEqual(code1, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(code2, status.HTTP_401_UNAUTHORIZED)

    def test_user(self):
        self.client.force_authenticate(user=self.user)
        code1, data1 = self._replies()
        code2, data2 = self._replies_count()
        self.assertEqual(code1, status.HTTP_200_OK)
        self.assertEqual(code2, status.HTTP_200_OK)


class BasicTestCaseUserMethodsPermissions(APITestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.creator = Creator.create_user(1)
        self.post = Creator.create_post(self.creator)

    def _url(self, method):
        return lib.url('user/' + str(self.user.id) + '/' + method + '/')

    def _help(self):
        url = self._url('help')
        condition = Creator.create_condition_dict(ConditionType.THANKS)
        data = {
            'post': self.post.id,
            'condition': condition
        }
        return self.client.post(url, data=data, format='json'), condition

    def test_method_help_not_creator(self):
        self.client.force_authenticate(user=self.user)
        response, condition = self._help()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_method_help_creator(self):
        self.client.force_authenticate(user=self.creator)
        response, condition = self._help()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_method_cancel_help_not_creator(self):
        try:
            self.client.force_authenticate(user=self.creator)
            self._help()
            self.client.force_authenticate(user=self.user)
            url = self._url('cancel-help')
            self.client.post(url, data={'post': self.post.id}, format='json')  # raise Help.DoesNotExist
            self.assertEqual(0, 1)
        except Help.DoesNotExist:
            pass

    def test_method_cancel_help_creator(self):
        self.client.force_authenticate(user=self.creator)
        self._help()
        url = self._url('cancel-help')
        response = self.client.post(url, data={'post': self.post.id}, format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_close_post_not_creator(self):
        self.client.force_authenticate(user=self.creator)
        self._help()
        self.client.force_authenticate(user=self.user)
        url = self._url('close')
        _response = self.client.post(url, data={'post': self.post.id}, format='json')
        self.assertEqual(_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_close_post_creator(self):
        self.client.force_authenticate(user=self.creator)
        self._help()
        url = self._url('close')
        _response = self.client.post(url, data={'post': self.post.id}, format='json')
        self.assertEqual(_response.status_code, status.HTTP_200_OK)

    def test_select_helper_not_creator(self):
        self.client.force_authenticate(user=self.user)
        self._help()
        url = self._url('select-helper')
        _response = self.client.post(url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        self.assertEqual(_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_select_helper_creator(self):
        self.client.force_authenticate(user=self.user)
        self._help()
        self.client.force_authenticate(user=self.creator)
        url = self._url('select-helper')
        _response = self.client.post(url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        self.assertEqual(_response.status_code, status.HTTP_200_OK)

    def test_cancel_helper_not_creator(self):
        self.client.force_authenticate(user=self.user)
        self._help()
        self.client.force_authenticate(user=self.creator)
        url = self._url('select-helper')
        self.client.post(url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        self.client.force_authenticate(user=self.user)
        _url = self._url('cancel-helper')
        _response = self.client.post(_url, data={'post': self.post.id, 'user': self.user.id}, format='json')

        self.assertEqual(_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_cancel_helper_creator(self):
        self.client.force_authenticate(user=self.user)
        self._help()
        self.client.force_authenticate(user=self.creator)
        url = self._url('select-helper')
        self.client.post(url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        _url = self._url('cancel-helper')
        _response = self.client.post(_url, data={'post': self.post.id, 'user': self.user.id}, format='json')

        self.assertEqual(_response.status_code, status.HTTP_200_OK)

    def test_select_successfully_not_creator(self):
        self.client.force_authenticate(user=self.user)
        self._help()
        self.client.force_authenticate(user=self.creator)
        url = self._url('select-helper')
        self.client.post(url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        self.client.force_authenticate(user=self.user)
        _url = self._url('select-successfully')
        _response = self.client.post(_url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        self.assertEqual(_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_select_successfully_skip_select_helper(self):
        try:
            self.client.force_authenticate(user=self.user)
            self._help()
            self.client.force_authenticate(user=self.creator)
            _url = self._url('select-successfully')
            self.client.post(_url, data={'post': self.post.id, 'user': self.user.id},
                             format='json')  # raise Help.DoesNotExist
            self.assertEqual(0, 1)
        except Help.DoesNotExist:
            pass


class BasicTestCaseLikePermission(APITestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.creator = Creator.create_user(1)
        self.post = Creator.create_post(self.creator)
        self.comment = Creator.create_comment_post(self.creator, self.post)

    def _url(self, method):
        return lib.url('user/' + str(self.user.id) + '/' + method + '/')

    def test_method_like_user(self):
        self.client.force_authenticate(user=self.user)
        url = self._url('like-comment-post')
        response = self.client.post(url, data={'comment': self.comment.id}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_method_like_anonymous(self):
        url = self._url('like-comment-post')
        response = self.client.post(url, data={'comment': self.comment.id}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
