from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from nmodels.models import User, Category, Contact, MediaType
from nmodels.serializers import UserDetailSerializer
from datetime import date
from nmodels import lib
from nproject.settings import REST_FRAMEWORK
from nmodels.tests.lib import Creator


class BasicTestCaseUserCommon(TestCase):

    def setUp(self):
        self.user = Creator.create_user()

    def test_control(self):
        self.assertEqual(User.objects.count(), 1)

    def test_filed_simple_equal(self):
        self.assertEqual(self.user.email, 'maksim@gmail.com')
        self.assertEqual(self.user.first_name, 'Maksim')
        self.assertEqual(self.user.second_name, 'Andrianov')
        self.assertEqual(self.user.like, 0)
        self.assertEqual(self.user.dislike, 0)
        self.assertNotEqual(self.user.created, None)
        self.assertNotEqual(self.user.updated, None)

    def test_filed_simple_additionally(self):
        self.user.bday = date(2000, 2, 2)
        self.user.gender = 'm'
        self.user.info = 'Я хороший! Умею читать и писать. Люблю покушать. Хочу помогать людям.'
        self.user.save()
        user = Creator.get_user()
        self.assertEqual(user.bday, date(2000, 2, 2))
        self.assertEqual(user.gender, 'm')
        self.assertEqual(user.info, 'Я хороший! Умею читать и писать. Люблю покушать. Хочу помогать людям.')

    def test_filed_is_not_none(self):
        self.assertEqual(self.user.categories is None, False)
        self.assertEqual(self.user.settings is None, False)

    def test_field_categories_number(self):
        c1 = Creator.create_category(1, 1)
        c2 = Creator.create_category(2, 2)
        self.user.categories.add(*[c1, c2])
        self.assertEqual(self.user.categories.count(), 2)

    def test_field_contacts_number(self):
        c1 = Creator.create_contact(1, '+79817068020')
        c2 = Creator.create_contact(2, 'test@test.ru')
        self.user.contacts.add(*[c1, c2])
        self.assertEqual(self.user.contacts.count(), 2)

    def test_field_update_time(self):
        datetime_before = self.user.updated
        self.user.save()
        self.assertGreater(self.user.updated, datetime_before)

    def test_method_see_replies(self):
        last_activity = self.user.activity
        self.user.see_replies()
        self.assertGreater(self.user.activity, last_activity)


class BasicTestCaseUserAddContacts(TestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.d_contacts = [
            Creator.create_contact_dict(1, '+79817068020'),
            Creator.create_contact_dict(2, 'test@test.ru'),
            Creator.create_contact_dict(3, 'https://vk.com/maxandr')
        ]

    def test_method_update_contacts_add_count(self):
        self.user._update_contacts(self.d_contacts)
        self.assertEqual(self.user.contacts.count(), 3)

    def test_method_update_contacts_add_data(self):
        self.user._update_contacts(self.d_contacts)
        ct1 = Contact.objects.get(type=1, data='+79817068020')
        ct2 = Contact.objects.get(type=2, data='test@test.ru')
        ct3 = Contact.objects.get(type=3, data='https://vk.com/maxandr')
        self.assertNotEqual(ct1.pk, None)
        self.assertNotEqual(ct2.pk, None)
        self.assertNotEqual(ct3.pk, None)

    def test_method_update_contacts_add_emty_data(self):
        self.user._update_contacts([])
        self.assertEqual(self.user.contacts.count(), 0)

    def test_method_update_contacts_del_data(self):
        self.user._update_contacts(self.d_contacts)
        ct_old = self.user.contacts.order_by('id')
        d_contacts = self.d_contacts[0:2]
        self.user._update_contacts(d_contacts)
        ct = self.user.contacts.order_by('id')
        self.assertEqual(self.user.contacts.count(), 2)
        self.assertEqual(ct_old[0].pk, ct[0].pk)
        self.assertEqual(ct_old[1].pk, ct[1].pk)

    def test_method_update_contacts_insert_data(self):
        self.user._update_contacts(self.d_contacts)
        ct_old = self.user.contacts.order_by('id')
        d_contacts = list(self.d_contacts)
        d_contacts.append(Creator.create_contact_dict(4, 'https://fb.com/maxandr'))
        self.user._update_contacts(d_contacts)
        ct = self.user.contacts.order_by('id')
        self.assertEqual(self.user.contacts.count(), 4)
        self.assertEqual(ct_old[0].pk, ct[0].pk)
        self.assertEqual(ct_old[1].pk, ct[1].pk)
        self.assertEqual(ct_old[2].pk, ct[2].pk)

    def test_method_update_contacts_insert_repeated_data(self):
        self.user._update_contacts(self.d_contacts)
        self.user._update_contacts(self.d_contacts)
        self.assertEqual(self.user.contacts.count(), 3)


class BasicTestCaseUserAddCategories(TestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.d_categories = [
            Creator.create_category_dict(1, 1),
            Creator.create_category_dict(2, 2),
            Creator.create_category_dict(3, 3)
        ]

    def test_method_update_categories_add_count(self):
        self.user._update_categories(self.d_categories)
        self.assertEqual(self.user.categories.count(), 3)

    def test_method_update_categories_add_data(self):
        self.user._update_categories(self.d_categories)
        ct1 = Category.objects.get(category_type=1, subcategory_type=1)
        ct2 = Category.objects.get(category_type=2, subcategory_type=2)
        ct3 = Category.objects.get(category_type=3, subcategory_type=3)
        self.assertNotEqual(ct1.pk, None)
        self.assertNotEqual(ct2.pk, None)
        self.assertNotEqual(ct3.pk, None)

    def test_method_update_categories_add_emty_data(self):
        self.user._update_categories([])
        self.assertEqual(self.user.categories.count(), 0)

    def test_method_update_categories_del_data(self):
        self.user._update_categories(self.d_categories)
        ct_old = self.user.categories.order_by('id')
        d_categories = self.d_categories[0:2]
        self.user._update_categories(d_categories)
        ct = self.user.categories.order_by('id')
        self.assertEqual(self.user.categories.count(), 2)
        self.assertEqual(ct_old[0].pk, ct[0].pk)
        self.assertEqual(ct_old[1].pk, ct[1].pk)

    def test_method_update_categories_insert_data(self):
        self.user._update_categories(self.d_categories)
        ct_old = self.user.categories.order_by('id')
        d_categories = list(self.d_categories)
        d_categories.append(Creator.create_category_dict(4, 4))
        self.user._update_categories(d_categories)
        ct = self.user.categories.order_by('id')
        self.assertEqual(self.user.categories.count(), 4)
        self.assertEqual(ct_old[0].pk, ct[0].pk)
        self.assertEqual(ct_old[1].pk, ct[1].pk)
        self.assertEqual(ct_old[2].pk, ct[2].pk)

    def test_method_update_categories_insert_repeated_data(self):
        self.user._update_categories(self.d_categories)
        self.user._update_categories(self.d_categories)
        self.assertEqual(self.user.categories.count(), 3)


class BasicTestCaseUserSerializer(TestCase):
    def setUp(self):
        self.user = Creator.create_user()
        self.d_categories = [
            Creator.create_category_dict(1, 1),
            Creator.create_category_dict(2, 2),
            Creator.create_category_dict(3, 3)
        ]
        self.d_contacts = [
            Creator.create_contact_dict(1, '+79817068020'),
            Creator.create_contact_dict(2, 'test@test.ru'),
            Creator.create_contact_dict(3, 'https://vk.com/maxandr')
        ]
        self.avatar = {
            'url': 'https://cdn02.nativeroll.tv/images/mc.jpg'
        }

    def _update_contacts_categories(self):
        data = {'contacts': self.d_contacts, 'categories': self.d_categories}
        serializer = UserDetailSerializer(self.user, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
        else:
            print('data=data', serializer.error_messages)

    def test_filed_simple_equal(self):
        serializer = UserDetailSerializer(self.user)
        data = serializer.data
        self.assertEqual(data.get('first_name'), 'Maksim')
        self.assertEqual(data.get('second_name'), 'Andrianov')
        self.assertEqual(data.get('contacts'), [])
        self.assertEqual(data.get('categories'), [])
        self.assertEqual(data.get('media'), [])
        self.assertEqual(data.get('gender'), 'x')
        self.assertEqual(data.get('info'), '')
        self.assertEqual(data.get('rank'), 0)
        self.assertEqual(data.get('bday'), None)
        self.assertEqual(data.get('avatar'), None)
        self.assertEqual(data.get('like'), 0)
        self.assertEqual(data.get('dislike'), 0)
        self.assertNotEqual(data.get('created'), None)
        self.assertNotEqual(data.get('updated'), None)

    def test_method_update_simple_add(self):
        data = {
            'first_name': 'Test',
            'second_name': 'Test',
            'contacts': self.d_contacts,
            'categories': self.d_categories,
            'avatar': self.avatar
        }
        serializer = UserDetailSerializer(self.user, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
        else:
            print(serializer.error_messages)

        self.assertEqual(self.user.first_name, 'Test')
        self.assertEqual(self.user.avatar.url, self.avatar['url'])
        self.assertEqual(self.user.avatar.type, MediaType.AVATAR)
        self.assertEqual(self.user.second_name, 'Test')
        self.assertEqual(self.user.contacts.count(), 3)
        self.assertEqual(self.user.categories.count(), 3)

    def test_method_update_add(self):
        self._update_contacts_categories()
        new_data = {
            'contacts': self.d_contacts + [Creator.create_contact_dict(4, 'https://fb.com/maxandr')],
            'categories': self.d_categories + [Creator.create_category_dict(4, 4)]
        }
        serializer = UserDetailSerializer(self.user, data=new_data, partial=True)
        if serializer.is_valid():
            serializer.save()
        else:
            print('data=new_data', serializer.error_messages)
        self.assertEqual(self.user.contacts.count(), 4)
        self.assertEqual(self.user.categories.count(), 4)

    def test_method_update_remove(self):
        self._update_contacts_categories()
        new_data = {
            'contacts': self.d_contacts[0:2],
            'categories': self.d_categories[0:2]
        }
        serializer = UserDetailSerializer(self.user, data=new_data, partial=True)
        if serializer.is_valid():
            serializer.save()
        else:
            print('data=new_data', serializer.error_messages)
        self.assertEqual(self.user.contacts.count(), 2)
        self.assertEqual(self.user.categories.count(), 2)


class BasicTestCaseUserView(APITestCase):

    def setUp(self):
        self.users = Creator.create_users(2)
        self.d_categories = [
            Creator.create_category_dict(1, 1),
            Creator.create_category_dict(2, 2),
            Creator.create_category_dict(3, 3)
        ]
        self.d_contacts = [
            Creator.create_contact_dict(1, '+79817068020'),
            Creator.create_contact_dict(2, 'test@test.ru'),
            Creator.create_contact_dict(3, 'https://vk.com/maxandr')
        ]
        self.avatar = {
            'url': 'https://cdn02.nativeroll.tv/images/mc.jpg'
        }
        self.client.force_authenticate(user=self.users[0])

    def test_users(self):
        url = lib.url('users/')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), REST_FRAMEWORK['PAGE_SIZE'])
        self.assertNotEqual(response.data['next'], None)
        self.assertEqual(response.data['previous'], None)

    def test_users_next(self):
        url = lib.url('users/')
        response = self.client.get(url)
        url = response.data['next']
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), REST_FRAMEWORK['PAGE_SIZE'])
        self.assertNotEqual(response.data['previous'], None)
        self.assertEqual(response.data['next'], None)

    def test_user_detail(self):
        url = lib.url('user/' + str(self.users[0].id) + '/')
        response = self.client.get(url)
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data.get('first_name'), 'Maksim')
        self.assertEqual(data.get('second_name'), 'Andrianov')
        self.assertEqual(data.get('contacts'), [])
        self.assertEqual(data.get('categories'), [])
        self.assertEqual(data.get('media'), [])
        self.assertEqual(data.get('gender'), 'x')
        self.assertEqual(data.get('info'), '')
        self.assertEqual(data.get('rank'), 0)
        self.assertEqual(data.get('bday'), None)
        self.assertEqual(data.get('like'), 0)
        self.assertEqual(data.get('dislike'), 0)
        self.assertNotEqual(data.get('created'), None)
        self.assertNotEqual(data.get('updated'), None)

    def test_user_update(self):
        url = lib.url('user/' + str(self.users[0].id) + '/')
        data = {
            'first_name': 'Test',
            'contacts': self.d_contacts,
            'categories': self.d_categories,
            'avatar': self.avatar
        }
        response = self.client.patch(url, data=data, format='json')
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data.get('first_name'), 'Test')
        self.assertEqual(data.get('second_name'), 'Andrianov')
        self.assertEqual(len(data.get('contacts')), 3)
        self.assertEqual(len(data.get('categories')), 3)
        self.assertEqual(data.get('media'), [])
        self.assertEqual(data.get('gender'), 'x')
        self.assertEqual(data.get('info'), '')
        self.assertEqual(data.get('rank'), 0)
        self.assertEqual(data.get('bday'), None)
        self.assertEqual(data.get('like'), 0)
        self.assertEqual(data.get('dislike'), 0)
        self.assertNotEqual(data.get('created'), None)
        self.assertNotEqual(data.get('updated'), None)

        self.assertEqual(data['avatar']['url'], self.avatar['url'])
        self.assertEqual(data['avatar']['type'], MediaType.AVATAR)
