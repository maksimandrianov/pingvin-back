import random
import os
import sys
from random_words import RandomNicknames
from datetime import date


def read_file_in_list(fname):
    with open(fname) as f:
        return f.readlines()

class RandomUser():

    def __init__(self):
        self.man_first_name = read_file_in_list('../dbdata/user_name_man.txt')
        self.man_second_name = read_file_in_list('../dbdata/user_sname_man.txt')
        self.woman_first_name = read_file_in_list('../dbdata/user_name_woman.txt')
        self.woman_second_name = read_file_in_list('../dbdata/user_sname_woman.txt')

    def create_gender_and_name(self):
        gender = random.randint(0, 1)
        if gender == 1:
            return ('m',
                    self._gfcoll(self.man_first_name),
                    self._gfcoll(self.man_second_name))
        else:
            return ('f',
                    self._gfcoll(self.woman_first_name),
                    self._gfcoll(self.woman_second_name))

    @staticmethod
    def create_email():
        lemail = (
            'gmail.com',
            'mail.ru',
            'yandex.ru',
        )
        email = RandomNicknames().random_nick(gender='u') + str(random.randint(0, 10000)) \
                + '@' + lemail[random.randint(0, len(lemail)-1)]
        return email

    @staticmethod
    def create_bday():
        return date(
                random.randint(1950, 2005),
                random.randint(1, 12),
                random.randint(1, 26))

    @staticmethod
    def create_number():
        return '8' + str(random.randint(100, 999)) +str(random.randint(100, 999)) \
               + str(random.randint(10, 99)) + str(random.randint(10, 99))

    def create_info(self):
        W1 = ('умею ', 'люблю ', 'обожаю ')
        W2 = ('печь пирожки', 'программировать сайты', 'делать маникюр людям', 'помогать людям',  'чинить машины')
        W3 = ('Так же ', 'Еще ', 'И ')
        W4 = ('немного ', 'хорошо ', 'превосходно ')
        W5 = ('разбираюсь в программировании', 'увлекаюсь плаванием', 'разбираюсь в юриспруденции', 'увлекаюсь экономикой')

        return 'Я ' + self._gfcoll(W1) + self._gfcoll(W2) + '. ' \
               + self._gfcoll(W3) + self._gfcoll(W4) + self._gfcoll(W5) + '.'

    @staticmethod
    def create_rank():
        return random.randint(0, 10)

    def create_category(self):

        lcategory= (
            'Кулинария',
            'Птицеводство',
            'Автодело'
        )

        lsubcategory = (
            'С++',
            'Развожу щенков',
            'Карбюраторы'
        )
        category = random.randint(0, 10)
        subcategory = random.randint(0, 10)
        category_text = ''
        subcategory_text = ''
        if category == 0:
            category_text = self._gfcoll(lcategory)
        if subcategory == 0:
            category_text = self._gfcoll(lsubcategory)
        return (category, category_text,
                subcategory, subcategory_text)

    def _gfcoll(self, coll):
        return coll[random.randint(0, len(coll)-1)].strip()


class RandomPost():

    def __init__(self, range_id_users):
        self.range_id_users = range_id_users

    def is_actually(self):
        return random.randint(0, 1)

    def created_users(self):
        return random.randint(self.range_id_users[0],
                              self.range_id_users[1])

    def header(self):
        W1 = ('Спасите ', 'Помогите ', 'Нужна помощь')
        W2 = ('угнали что то', 'своровали что-то', 'сломал ногу', 'зашлох на шоссе',  'нужен курьер', 'нужно поставить винду')
        return self._gfcoll(W1) + ', ' + self._gfcoll(W2) + '.'

    def text(self):
        return 'Вновь кажется, что мне не быть иным, лишенным унизительных стандартов;' + \
                  'и над пытливым образом моим' + \
                  'старательно продуманным, раз в квартал' + \
                  'выказывает время торжество' + \
                  'с присущем многопудьем многоточий...' + \
                  'Напитываюсь майскою листвой' + \
                  'как изначально был уполномочен'

    def req_number_of_helpers(self):
        return random.randint(4, 5)

    def geo(self):
        return 1, 1, (59.925264,30.286743), 'Проспект ветеранов, 7'

    def users(self):
        res = []
        n = random.randint(1, 3)
        for i in range(n):
            res.append(random.randint(self.range_id_users[0],
                                      self.range_id_users[1]))
        return res

    def _gfcoll(self, coll):
        return coll[random.randint(0, len(coll)-1)]


NUMBER_OF_USERS = 100
NUMBER_OF_POST = 50

if __name__ == '__main__':
    sys.path.insert(0, '/home/maks/Desktop/Learn/python-env/model_env/nproject')
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nproject.settings')
    import django
    django.setup()
    from nmodels.models import User, Category
    random_user = RandomUser()
    for i in range(NUMBER_OF_USERS):
        gender, fname, sname = random_user.create_gender_and_name()
        user = User.objects.create(
            email=random_user.create_email(),
            password='********',
            first_name=fname,
            second_name =sname,
            bday=random_user.create_bday(),
            rank=random_user.create_rank(),
            gender=gender,
            info=random_user.create_info(),
        )
        n = random.randint(1, 5)
        for i in range(n):
            category, category_text, subcategory, subcategory_text = random_user.create_category()
            category = Category.objects.create(
                    category_type=category,
                    other_category_name=category_text,
                    subcategory_type=subcategory,
                    other_subcategory_name=subcategory_text
            )
            user.categories.add(category)
    from nmodels.models import GeoInfo, Post, Condition, Help
    from django.contrib.gis.geos import Point
    random_post = RandomPost((27, 27 + NUMBER_OF_POST))
    for i in range(NUMBER_OF_POST):
        geo_inf = random_post.geo()
        post = Post.objects.create(
            is_actually=random_post.is_actually(),
            created_user=User.objects.get(pk=random_post.created_users()),
            header=random_post.header(),
            text=random_post.text(),
            req_helpers=random_post.req_number_of_helpers(),
            location = GeoInfo.objects.create(
                    point=Point(geo_inf[2][0], geo_inf[2][1]),
                    address=geo_inf[3]
            ),
            condition = Condition.objects.create(type=2, other='Могу угостить пирожками'),
        )
        users = random_post.users()
        for user in users:
            Help.objects.create(
                    post=post,
                    user=User.objects.get(pk=user),
                    condition=Condition.objects.create(type=2, other='Могу угостить пирожками'),
            )
