from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from nmodels.models import LikePost, LikeCommentPost, LikeCommentUser
from nmodels import lib
from nmodels.tests.lib import Creator


class BasicTestCaseLikePostCommon(TestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.creator = Creator.create_user(1)
        self.post = Creator.create_post(self.creator)

    def test_method_like1(self):
        self.user.like_post(self.post.id)

        post = Creator.get_post(self.post.id)
        ld = LikePost.objects.get(user=self.user, obj=self.post)

        self.assertEqual(post.like, 1)
        self.assertEqual(post.dislike, 0)
        self.assertEqual(ld.is_like, True)

    def test_method_like2(self):
        self.user.like_post(self.post.id)
        self.user.like_post(self.post.id)

        post = Creator.get_post(self.post.id)
        ld = LikePost.objects.get(user=self.user, obj=self.post)

        self.assertEqual(post.like, 0)
        self.assertEqual(post.dislike, 0)
        self.assertEqual(ld.is_like, None)

    def test_method_like3(self):
        self.user.like_post(self.post.id)
        self.user.dislike_post(self.post.id)

        post = Creator.get_post(self.post.id)
        ld = LikePost.objects.get(user=self.user, obj=self.post)

        self.assertEqual(post.like, 0)
        self.assertEqual(post.dislike, 1)
        self.assertEqual(ld.is_like, False)

    def test_method_dislike1(self):
        self.user.dislike_post(self.post.id)

        post = Creator.get_post(self.post.id)
        ld = LikePost.objects.get(user=self.user, obj=self.post)

        self.assertEqual(post.like, 0)
        self.assertEqual(post.dislike, 1)
        self.assertEqual(ld.is_like, False)

    def test_method_dislike2(self):
        self.user.dislike_post(self.post.id)
        self.user.dislike_post(self.post.id)

        post = Creator.get_post(self.post.id)
        ld = LikePost.objects.get(user=self.user, obj=self.post)

        self.assertEqual(post.like, 0)
        self.assertEqual(post.dislike, 0)
        self.assertEqual(ld.is_like, None)

    def test_method_dislike3(self):
        self.user.dislike_post(self.post.id)
        self.user.like_post(self.post.id)

        post = Creator.get_post(self.post.id)
        ld = LikePost.objects.get(user=self.user, obj=self.post)

        self.assertEqual(post.like, 1)
        self.assertEqual(post.dislike, 0)
        self.assertEqual(ld.is_like, True)


class BasicTestCaseLikeCommentPostCommon(TestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.creator = Creator.create_user(1)
        self.post = Creator.create_post(self.creator)
        self.comment = Creator.create_comment_post(self.creator, self.post)

    def test_method_like1(self):
        self.user.like_comment_post(self.comment.id)

        comment = Creator.get_comment_post(self.comment.id)
        ld = LikeCommentPost.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 1)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(ld.is_like, True)

    def test_method_like2(self):
        self.user.like_comment_post(self.comment.id)
        self.user.like_comment_post(self.comment.id)

        comment = Creator.get_comment_post(self.comment.id)
        ld = LikeCommentPost.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(ld.is_like, None)

    def test_method_like3(self):
        self.user.like_comment_post(self.comment.id)
        self.user.dislike_comment_post(self.comment.id)

        comment = Creator.get_comment_post(self.comment.id)
        ld = LikeCommentPost.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 1)
        self.assertEqual(ld.is_like, False)

    def test_method_dislike1(self):
        self.user.dislike_comment_post(self.comment.id)

        comment = Creator.get_comment_post(self.comment.id)
        ld = LikeCommentPost.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 1)
        self.assertEqual(ld.is_like, False)

    def test_method_dislike2(self):
        self.user.dislike_comment_post(self.comment.id)
        self.user.dislike_comment_post(self.comment.id)

        comment = Creator.get_comment_post(self.comment.id)
        ld = LikeCommentPost.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(ld.is_like, None)

    def test_method_dislike3(self):
        self.user.dislike_comment_post(self.comment.id)
        self.user.like_comment_post(self.comment.id)

        comment = Creator.get_comment_post(self.comment.id)
        ld = LikeCommentPost.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 1)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(ld.is_like, True)


class BasicTestCaseLikeCommentUserCommon(TestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.creator = Creator.create_user(1)
        self.comment = Creator.create_comment_user(self.creator, self.user)

    def test_method_like1(self):
        self.user.like_comment_user(self.comment.id)

        comment = Creator.get_comment_user(self.comment.id)
        ld = LikeCommentUser.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 1)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(ld.is_like, True)

    def test_method_like2(self):
        self.user.like_comment_user(self.comment.id)
        self.user.like_comment_user(self.comment.id)

        comment = Creator.get_comment_user(self.comment.id)
        ld = LikeCommentUser.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(ld.is_like, None)

    def test_method_like3(self):
        self.user.like_comment_user(self.comment.id)
        self.user.dislike_comment_user(self.comment.id)

        comment = Creator.get_comment_user(self.comment.id)
        ld = LikeCommentUser.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 1)
        self.assertEqual(ld.is_like, False)

    def test_method_dislike1(self):
        self.user.dislike_comment_user(self.comment.id)

        comment = Creator.get_comment_user(self.comment.id)
        ld = LikeCommentUser.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 1)
        self.assertEqual(ld.is_like, False)

    def test_method_dislike2(self):
        self.user.dislike_comment_user(self.comment.id)
        self.user.dislike_comment_user(self.comment.id)

        comment = Creator.get_comment_user(self.comment.id)
        ld = LikeCommentUser.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(ld.is_like, None)

    def test_method_dislike3(self):
        self.user.dislike_comment_user(self.comment.id)
        self.user.like_comment_user(self.comment.id)

        comment = Creator.get_comment_user(self.comment.id)
        ld = LikeCommentUser.objects.get(user=self.user, obj=self.comment)

        self.assertEqual(comment.like, 1)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(ld.is_like, True)


class BasicTestCaseLikePostView(APITestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.creator = Creator.create_user(1)
        self.post = Creator.create_post(self.creator)
        self.client.force_authenticate(user=self.user)

    def _url(self, method):
        return lib.url('user/' + str(self.user.id) + '/' + method + '/')

    def test_method_like(self):
        url = self._url('like-post')
        response = self.client.post(url, data={'post': self.post.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], True)

    def test_method_dislike(self):
        url = self._url('dislike-post')
        response = self.client.post(url, data={'post': self.post.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], False)

    def test_method_like_like(self):
        url = self._url('like-post')
        self.client.post(url, data={'post': self.post.id}, format='json')
        response = self.client.post(url, data={'post': self.post.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], None)

    def test_method_dislike_dislike(self):
        url = self._url('dislike-post')
        self.client.post(url, data={'post': self.post.id}, format='json')
        response = self.client.post(url, data={'post': self.post.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], None)


class BasicTestCaseLikeCommentPostView(APITestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.creator = Creator.create_user(1)
        self.post = Creator.create_post(self.creator)
        self.comment = Creator.create_comment_post(self.creator, self.post)
        self.client.force_authenticate(user=self.user)

    def _url(self, method):
        return lib.url('user/' + str(self.user.id) + '/' + method + '/')

    def test_method_like(self):
        url = self._url('like-comment-post')
        response = self.client.post(url, data={'comment': self.comment.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], True)

    def test_method_dislike(self):
        url = self._url('dislike-comment-post')
        response = self.client.post(url, data={'comment': self.comment.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], False)

    def test_method_like_like(self):
        url = self._url('like-comment-post')
        self.client.post(url, data={'comment': self.comment.id}, format='json')
        response = self.client.post(url, data={'comment': self.comment.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], None)

    def test_method_dislike_dislike(self):
        url = self._url('dislike-comment-post')
        self.client.post(url, data={'comment': self.comment.id}, format='json')
        response = self.client.post(url, data={'comment': self.comment.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], None)


class BasicTestCaseLikeCommentUserView(APITestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.creator = Creator.create_user(1)
        self.comment = Creator.create_comment_user(self.creator, self.user)
        self.client.force_authenticate(user=self.user)

    def _url(self, method):
        return lib.url('user/' + str(self.user.id) + '/' + method + '/')

    def test_method_like(self):
        url = self._url('like-comment-user')
        response = self.client.post(url, data={'comment': self.comment.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], True)

    def test_method_dislike(self):
        url = self._url('dislike-comment-user')
        response = self.client.post(url, data={'comment': self.comment.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], False)

    def test_method_like_like(self):
        url = self._url('like-comment-user')
        self.client.post(url, data={'comment': self.comment.id}, format='json')
        response = self.client.post(url, data={'comment': self.comment.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], None)

    def test_method_dislike_dislike(self):
        url = self._url('dislike-comment-user')
        self.client.post(url, data={'comment': self.comment.id}, format='json')
        response = self.client.post(url, data={'comment': self.comment.id}, format='json')
        l = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(l['like'], None)