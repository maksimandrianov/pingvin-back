from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from nmodels.models import CommentPost, CommentUser, ConditionType, Help
from nmodels.serializers import PostCommentsSerializer, UserCommentsSerializer
from nmodels import lib
from nproject.settings import REST_FRAMEWORK
from nmodels.tests.lib import Creator, Request, View


class BasicTestCaseCommentPostCommon(TestCase):

    def setUp(self):
        self.created_user = Creator.create_user()
        self.answered_him = Creator.create_user(1)
        self.post = Creator.create_post(self.created_user)
        self.comment = Creator.create_comment_post(self.created_user, self.post)

    def _comment(self):
       return Creator.get_comment_post(self.comment.pk)

    def test_control(self):
        self.assertEqual(CommentPost.objects.count(), 1)

    def test_filed_simple_equal(self):
        self.assertEqual(self.comment.created_user.id, self.created_user.id)
        self.assertEqual(self.comment.text, 'This is test post comment text')
        self.assertEqual(self.comment.like, 0)
        self.assertEqual(self.comment.dislike, 0)
        self.assertNotEqual(self.comment.created, None)
        self.assertNotEqual(self.comment.updated, None)

    def test_filed_simple_additionally(self):
        self.comment.answered_him = self.answered_him
        self.comment.save()
        comment = self._comment()
        self.assertEqual(self.comment.answered_him.id, comment.answered_him.id)


class BasicTestCaseCommentUserCommon(TestCase):

    def setUp(self):
        self.created_user = Creator.create_user()
        self.answered_him = Creator.create_user(1)
        self.user = Creator.create_user(2)
        self.comment = Creator.create_comment_user(self.created_user, self.user)

    def _comment(self):
       return Creator.get_comment_user(self.comment.pk)

    def test_control(self):
        self.assertEqual(CommentUser.objects.count(), 1)

    def test_filed_simple_equal(self):
        self.assertEqual(self.comment.created_user.id, self.created_user.id)
        self.assertEqual(self.comment.text, 'This is test user comment text')
        self.assertEqual(self.comment.like, 0)
        self.assertEqual(self.comment.dislike, 0)
        self.assertEqual(self.comment.rating, 5)
        self.assertNotEqual(self.comment.created, None)
        self.assertNotEqual(self.comment.updated, None)

    def test_filed_simple_additionally(self):
        self.comment.answered_him = self.answered_him
        self.comment.save()
        comment = self._comment()
        self.assertEqual(self.comment.answered_him.id, comment.answered_him.id)


class BasicTestCaseCommentPostCommonSerializer(TestCase):

    def setUp(self):
        self.created_user = Creator.create_user()
        self.answered_him = Creator.create_user(1)
        self.post = Creator.create_post(self.created_user)
        self.comment = Creator.create_comment_post(self.created_user, self.post)

    def test_filed_simple_equal(self):
        request = Request(self.created_user, {})
        serializer = PostCommentsSerializer(self.comment, context={'request': request})
        data = serializer.data
        self.assertEqual(data['text'], 'This is test post comment text')
        self.assertEqual(data['like'], 0)
        self.assertEqual(data['dislike'], 0)
        self.assertEqual(data['answered_him'], None)
        self.assertEqual(data['created_user']['first_name'], 'Maksim')
        self.assertEqual(data['created_user']['second_name'], 'Andrianov')
        self.assertNotEqual(data['created'], None)
        # self.assertNotEqual(data['updated'], None)

    def test_method_create(self):
        data = {
            'text': 'Test text',
        }
        request = Request(self.created_user, data)
        view = View({'id_post': self.post.id})
        serializer = PostCommentsSerializer(data=data, context={'request': request, 'view': view})
        if serializer.is_valid():
            serializer.create(serializer.validated_data)
        else:
            print(serializer.error_messages)

        comment = CommentPost.objects.get(text='Test text')

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(comment.created_user.id, self.created_user.id)
        self.assertNotEqual(comment.created, None)
        self.assertNotEqual(comment.updated, None)

    def test_method_create_with_answered_him(self):
        data = {
            'text': 'Test text',
            'answered_him': {'id': self.answered_him.id}
        }
        request = Request(self.created_user, data, 'POST')
        view = View({'id_post': self.post.id})
        serializer = PostCommentsSerializer(data=data, context={'request': request, 'view': view})
        if serializer.is_valid():
            serializer.create(serializer.validated_data)
        else:
            print(serializer.error_messages)

        comment = CommentPost.objects.get(text='Test text')

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(comment.answered_him.id, self.answered_him.id)
        self.assertEqual(comment.created_user.id, self.created_user.id)
        self.assertNotEqual(comment.created, None)
        self.assertNotEqual(comment.updated, None)

    def test_method_after_like(self):
        self.created_user.like_comment_post(self.comment.id)
        request = Request(self.created_user, {})
        serializer = PostCommentsSerializer(self.comment, context={'request': request})
        data = serializer.data
        self.assertEqual(data.get('is_like'), True)

    def test_method_after_dislike(self):
        self.created_user.dislike_comment_post(self.comment.id)
        request = Request(self.created_user, {})
        serializer = PostCommentsSerializer(self.comment, context={'request': request})
        data = serializer.data
        self.assertEqual(data.get('is_like'), False)


class BasicTestCaseCommentUserCommonSerializer(APITestCase):

    def setUp(self):
        self.created_user = Creator.create_user()
        self.answered_him = Creator.create_user(1)
        self.user = Creator.create_user(2)
        self.comment = Creator.create_comment_user(self.created_user, self.user)
        self.post = Creator.create_post(self.user)

    def test_filed_simple_equal(self):
        request = Request(self.created_user, {})
        serializer = UserCommentsSerializer(self.comment, context={'request': request})
        data = serializer.data
        self.assertEqual(data['text'], 'This is test user comment text')
        self.assertEqual(data['like'], 0)
        self.assertEqual(data['dislike'], 0)
        self.assertEqual(data['rating'], 5)
        self.assertEqual(data['answered_him'], None)
        self.assertEqual(data['created_user']['first_name'], 'Maksim')
        self.assertEqual(data['created_user']['second_name'], 'Andrianov')
        self.assertNotEqual(data['created'], None)
        # self.assertNotEqual(data['updated'], None)

    def test_method_create(self):
        data = {
            'text': 'Test text',
            'rating': 4,
        }
        request = Request(self.created_user, data)
        view = View({'id_user': self.user.id})
        serializer = UserCommentsSerializer(data=data, context={'request': request, 'view': view})
        if serializer.is_valid():
            serializer.create(serializer.validated_data)
        else:
            print(serializer.error_messages)

        comment = CommentUser.objects.get(text='Test text')

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(comment.rating, 4)
        self.assertEqual(comment.created_user.id, self.created_user.id)
        self.assertNotEqual(comment.created, None)
        self.assertNotEqual(comment.updated, None)

    def test_method_create_with_answered_him(self):

        condition = Creator.create_condition_dict(ConditionType.THANKS)
        self.created_user.help(self.post.id, condition)
        self.user.select_helper(self.post.id, self.created_user.id)
        self.user.select_successfully(self.post.id, self.created_user.id)

        data = {
            'text': 'Test text',
            'rating': 4,
            'answered_him': {'id': self.answered_him.id}
        }
        view = View({'id_user': self.user.id})
        request = Request(self.created_user, data, 'POST')
        serializer = UserCommentsSerializer(data=data, context={'request': request, 'view': view})
        if serializer.is_valid():
            serializer.create(serializer.validated_data)
        else:
            print(serializer.error_messages)

        comment = CommentUser.objects.get(text='Test text')

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(comment.rating, 4)
        self.assertEqual(comment.answered_him.id, self.answered_him.id)
        self.assertEqual(comment.created_user.id, self.created_user.id)
        self.assertNotEqual(comment.created, None)
        self.assertNotEqual(comment.updated, None)

    def test_method_after_like(self):
        self.created_user.like_comment_user(self.comment.id)
        request = Request(self.created_user, {})
        serializer = UserCommentsSerializer(self.comment, context={'request': request})
        data = serializer.data
        self.assertEqual(data.get('is_like'), True)

    def test_method_after_dislike(self):
        self.created_user.dislike_comment_user(self.comment.id)
        request = Request(self.created_user, {})
        serializer = UserCommentsSerializer(self.comment, context={'request': request})
        data = serializer.data
        self.assertEqual(data.get('is_like'), False)


class BasicTestCasePostCommentView(APITestCase):

    def setUp(self):
        self.created_user = Creator.create_user()
        self.answered_him = Creator.create_user(1)
        self.post = Creator.create_post(self.created_user)
        self.comment = Creator.create_comments_post(2, self.post, self.created_user)
        self.client.force_authenticate(user=self.created_user)

    def test_post_comments(self):
        url = lib.url('post-comments/' + str(self.post.id) + '/')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), REST_FRAMEWORK['PAGE_SIZE'])
        self.assertNotEqual(response.data['next'], None)
        self.assertEqual(response.data['previous'], None)

    def test_post_comments_next(self):
        url = lib.url('post-comments/' + str(self.post.id) + '/')
        response = self.client.get(url)
        url = response.data['next']
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), REST_FRAMEWORK['PAGE_SIZE'])
        self.assertNotEqual(response.data['previous'], None)
        self.assertEqual(response.data['next'], None)

    def test_post_comment_create(self):
        url = lib.url('post-comments/' + str(self.post.id) + '/')
        data = {
            'text': 'Test text',
            'answered_him': {'id': self.answered_him.id}
        }
        response = self.client.post(url, data=data, format='json')
        commentt = response.data
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(commentt['text'], 'Test text')

        comment = CommentPost.objects.get(text='Test text')

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(comment.answered_him.id, self.answered_him.id)
        self.assertEqual(comment.created_user.id, self.created_user.id)
        self.assertNotEqual(comment.created, None)
        self.assertNotEqual(comment.updated, None)


class BasicTestCaseUserCommentView(APITestCase):

    def setUp(self):
        self.created_user = Creator.create_user()
        self.answered_him = Creator.create_user(1)
        self.user = Creator.create_user(2)
        self.comment = Creator.create_comments_user(2, self.user, self.created_user)
        self.post = Creator.create_post(self.user)
        self.client.force_authenticate(user=self.created_user)

    def test_user_comments(self):
        url = lib.url('user-comments/' + str(self.user.id) + '/')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), REST_FRAMEWORK['PAGE_SIZE'])
        self.assertNotEqual(response.data['next'], None)
        self.assertEqual(response.data['previous'], None)

    def test_user_comments_next(self):
        url = lib.url('user-comments/' + str(self.user.id) + '/')
        response = self.client.get(url)
        url = response.data['next']
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), REST_FRAMEWORK['PAGE_SIZE'])
        self.assertNotEqual(response.data['previous'], None)
        self.assertEqual(response.data['next'], None)

    def test_user_comment_create(self):

        condition = Creator.create_condition_dict(ConditionType.THANKS)
        self.created_user.help(self.post.id, condition)
        self.user.select_helper(self.post.id, self.created_user.id)
        self.user.select_successfully(self.post.id, self.created_user.id)

        url = lib.url('user-comments/' + str(self.user.id) + '/')
        data = {
            'text': 'Test text',
            'rating': 4,
            'answered_him': {'id': self.answered_him.id}
        }
        response = self.client.post(url, data=data, format='json')
        commentt = response.data
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(commentt['text'], 'Test text')

        comment = CommentUser.objects.get(text='Test text')

        self.assertEqual(comment.like, 0)
        self.assertEqual(comment.dislike, 0)
        self.assertEqual(comment.rating, 4)
        self.assertEqual(comment.answered_him.id, self.answered_him.id)
        self.assertEqual(comment.created_user.id, self.created_user.id)
        self.assertNotEqual(comment.created, None)
        self.assertNotEqual(comment.updated, None)


class BasicTestCaseSelectSuccessfullyView(APITestCase):

    def setUp(self):
        self.created_user = Creator.create_user()
        self.answered_him = Creator.create_user(1)
        self.user = Creator.create_user(2)
        self.comment = Creator.create_comments_user(2, self.user, self.created_user)
        self.post = Creator.create_post(self.user)
        self.client.force_authenticate(user=self.created_user)
        condition = Creator.create_condition_dict(ConditionType.THANKS)
        self.created_user.help(self.post.id, condition)
        self.user.select_helper(self.post.id, self.created_user.id)

    def test_cselect_successfully_req_atomic_valid(self):
        before = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.client.force_authenticate(user=self.user)
        url = lib.url('user/' + str(self.user.id) + '/cselect-successfully/')

        data = {
            'user': self.created_user.id,
            'post': self.post.id,
            'comment': {
                'rating': 5,
                'text': 'text'
            }
        }
        response = self.client.post(url, data=data, format='json')
        after = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.assertNotEqual(before.status, after.status)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cselect_successfully_req_atomic_without_text(self):
        before = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.client.force_authenticate(user=self.user)
        url = lib.url('user/' + str(self.user.id) + '/cselect-successfully/')

        data = {
            'user': self.created_user.id,
            'post': self.post.id,
            'comment': {
                'rating': 5,
            }
        }
        response = self.client.post(url, data=data, format='json')
        after = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.assertNotEqual(before.status, after.status)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cselect_successfully_req_atomic_min_bad_rating(self):
        before = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.client.force_authenticate(user=self.user)
        url = lib.url('user/' + str(self.user.id) + '/cselect-successfully/')

        data = {
            'user': self.created_user.id,
            'post': self.post.id,
            'comment': {
                'rating': 0,
            }
        }
        response = self.client.post(url, data=data, format='json')
        after = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.assertEqual(before.status, after.status)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cselect_successfully_req_atomic_max_bad_rating(self):
        before = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.client.force_authenticate(user=self.user)
        url = lib.url('user/' + str(self.user.id) + '/cselect-successfully/')

        data = {
            'user': self.created_user.id,
            'post': self.post.id,
            'comment': {
                'rating': 6,
            }
        }
        response = self.client.post(url, data=data, format='json')
        after = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.assertEqual(before.status, after.status)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cselect_successfully_req_atomic_not_valid_comment(self):
        before = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.client.force_authenticate(user=self.user)
        url = lib.url('user/' + str(self.user.id) + '/cselect-successfully/')

        data = {
            'user': self.created_user.id,
            'post': self.post.id,
            'comment': {
            }
        }
        response = self.client.post(url, data=data, format='json')
        after = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.assertEqual(before.status, after.status)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cselect_successfully_permission(self):
        before = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        url = lib.url('user/' + str(self.user.id) + '/cselect-successfully/')

        data = {
            'user': self.created_user.id,
            'post': self.post.id,
            'comment': {
                'rating': 5,
                'text': 'text'
            }
        }
        response = self.client.post(url, data=data, format='json')

        after = Help.objects.get(post_id=self.post.id, user_id=self.created_user.id)
        self.assertEqual(before.status, after.status)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)