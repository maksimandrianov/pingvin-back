from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from nmodels.models import Help, Condition, ConditionType, StatusType
from nmodels import lib
from nproject.settings import REST_FRAMEWORK
from nmodels.tests.lib import Creator


class BasicTestCaseHelpCommon(TestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.creator = Creator.create_user(1)
        self.post = Creator.create_post(self.creator)

    def _help(self):
        condition = Creator.create_condition_dict(ConditionType.THANKS)
        help = self.user.help(self.post.id, condition)
        return help, condition

    def test_method_help(self):
        help, condition = self._help()
        post = Creator.get_post(self.post.id)
        self.assertEqual(post.helpers, 1)
        self.assertEqual(help.post_id, self.post.id)
        self.assertEqual(help.user_id, self.user.id)
        self.assertEqual(help.is_selected, False)
        self.assertEqual(help.is_finish_help, False)
        self.assertEqual(help.status, StatusType.NEW_HELPER)
        self.assertEqual(help.condition.type, condition['type'])

    def test_method_cancel_help(self):
        self._help()
        count = Condition.objects.count()
        self.user.cancel_help(self.post.id)
        post = Creator.get_post(self.post.id)
        self.assertEqual(count, 1)
        self.assertEqual(post.helpers, 0)
        self.assertEqual(Help.objects.count(), 0)
        self.assertEqual(Condition.objects.count(), 0)

    def test_method_update_condition_help(self):
        help, condition = self._help()
        _condition = Creator.create_condition_dict(ConditionType.MONEY, 100)
        _help = self.user.update_condition_help(self.post.id, _condition)
        self.assertEqual(_help.status, StatusType.UPDATE_CONDITION)
        self.assertEqual(_help.condition.type, _condition['type'])
        self.assertEqual(_help.condition.money, _condition['money'])
        self.assertEqual(_help.condition.other, _condition['other'])

    def test_close_post(self):
        help, condition = self._help()
        post = self.creator.close(self.post.id)
        self.assertEqual(post.is_actually, False)

    def test_select_helper(self):
        help, condition = self._help()
        _help = self.creator.select_helper(self.post.id, self.user.id)
        self.assertEqual(_help.post_id, self.post.id)
        self.assertEqual(_help.user_id, self.user.id)
        self.assertEqual(_help.is_selected, True)
        self.assertEqual(_help.is_finish_help, False)
        self.assertEqual(_help.status, StatusType.SELECT_HELPER)
        self.assertEqual(_help.condition.type, condition['type'])

    def test_cancel_helper(self):
        help, condition = self._help()
        _help = self.creator.select_helper(self.post.id, self.user.id)
        __help = self.creator.cancel_helper(self.post.id, self.user.id)

        self.assertEqual(__help.post_id, self.post.id)
        self.assertEqual(__help.user_id, self.user.id)
        self.assertEqual(__help.is_selected, False)
        self.assertEqual(__help.is_finish_help, False)
        self.assertEqual(__help.status, StatusType.CANCEL_HELPER)
        self.assertEqual(__help.condition.type, condition['type'])

    def test_select_successfully(self):
        help, condition = self._help()
        _help = self.creator.select_helper(self.post.id, self.user.id)
        __help = self.creator.select_successfully(self.post.id, self.user.id)

        self.assertEqual(__help.post_id, self.post.id)
        self.assertEqual(__help.user_id, self.user.id)
        self.assertEqual(__help.is_selected, True)
        self.assertEqual(__help.is_finish_help, True)
        self.assertEqual(__help.status, StatusType.FINIS_HELP)
        self.assertEqual(__help.condition.type, condition['type'])


class BasicTestCaseHelpView(APITestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.creator = Creator.create_user(1)
        self.post = Creator.create_post(self.creator)
        self.client.force_authenticate(user=self.user)

    def _url(self, method):
        return lib.url('user/' + str(self.user.id) + '/' + method + '/')

    def _help(self):
        url = self._url('help')
        condition = Creator.create_condition_dict(ConditionType.THANKS)
        data = {
            'post': self.post.id,
            'condition': condition
        }
        return self.client.post(url, data=data, format='json'), condition

    def test_method_help(self):
        response, condition = self._help()
        help = response.data
        post = Creator.get_post(self.post.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(post.helpers, 1)
        self.assertEqual(help['user']['id'], self.user.id)
        self.assertEqual(help['is_selected'], False)
        self.assertEqual(help['is_finish_help'], False)
        self.assertEqual(help['status'], StatusType.NEW_HELPER)
        self.assertEqual(help['condition']['type'], condition['type'])

    def test_method_cancel_help(self):
        self._help()
        url = self._url('cancel-help')
        response = self.client.post(url, data={'post': self.post.id}, format='json')
        post = Creator.get_post(self.post.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(post.helpers, 0)
        self.assertEqual(Help.objects.count(), 0)
        self.assertEqual(Condition.objects.count(), 0)

    def test_method_update_condition_help(self):
        self._help()
        url = self._url('update-condition-help')
        _condition = Creator.create_condition_dict(ConditionType.MONEY, 100)
        response = self.client.post(url, data={'post': self.post.id, 'condition':_condition},
                                    format='json')
        help = response.data
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(help['condition']['type'], _condition['type'])
        self.assertEqual(help['condition']['money'], _condition['money'])
        self.assertEqual(help['condition']['other'], _condition['other'])

    def test_close_post(self):
        response, condition = self._help()
        self.client.force_authenticate(user=self.creator)
        url = self._url('close')
        _response = self.client.post(url, data={'post': self.post.id}, format='json')
        post = _response.data
        self.assertEqual(_response.status_code, status.HTTP_200_OK)
        self.assertEqual(post['is_actually'], False)

    def test_select_helper(self):
        response, condition = self._help()
        self.client.force_authenticate(user=self.creator)
        url = self._url('select-helper')
        _response = self.client.post(url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        help = _response.data

        self.assertEqual(_response.status_code, status.HTTP_200_OK)
        self.assertEqual(help['user']['id'], self.user.id)
        self.assertEqual(help['is_selected'], True)
        self.assertEqual(help['is_finish_help'], False)
        self.assertEqual(help['status'], StatusType.SELECT_HELPER)
        self.assertEqual(help['condition']['type'], condition['type'])

    def test_cancel_helper(self):
        response, condition = self._help()
        self.client.force_authenticate(user=self.creator)
        url = self._url('select-helper')
        self.client.post(url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        _url = self._url('cancel-helper')
        _response = self.client.post(_url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        help = _response.data

        self.assertEqual(_response.status_code, status.HTTP_200_OK)
        self.assertEqual(help['user']['id'], self.user.id)
        self.assertEqual(help['is_selected'], False)
        self.assertEqual(help['is_finish_help'], False)
        self.assertEqual(help['status'], StatusType.CANCEL_HELPER)
        self.assertEqual(help['condition']['type'], condition['type'])

    def test_select_successfully(self):
        response, condition = self._help()
        self.client.force_authenticate(user=self.creator)
        url = self._url('select-helper')
        r = self.client.post(url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        _url = self._url('select-successfully')
        _response = self.client.post(_url, data={'post': self.post.id, 'user': self.user.id}, format='json')
        help = _response.data
        self.assertEqual(_response.status_code, status.HTTP_200_OK)
        self.assertEqual(help['user']['id'], self.user.id)
        self.assertEqual(help['is_selected'], True)
        self.assertEqual(help['is_finish_help'], True)
        self.assertEqual(help['status'], StatusType.FINIS_HELP)
        self.assertEqual(help['condition']['type'], condition['type'])


class BasicTestCaseHelpsView(APITestCase):

    def setUp(self):
        self.creator = Creator.create_user(100)
        self.user = Creator.create_user(101)
        self.users = Creator.create_users(2)
        self.post = Creator.create_post(self.creator)
        self.client.force_authenticate(user=self.user)

        condition = Creator.create_condition_dict(ConditionType.THANKS)
        for user in self.users:
            user.help(self.post.id, condition)

    def test_helpers(self):
        url = lib.url('helpers/' + str(self.post.id) + '/')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), REST_FRAMEWORK['PAGE_SIZE'])
        self.assertNotEqual(response.data['next'], None)
        self.assertEqual(response.data['previous'], None)

    def test_helpers_next(self):
        url = lib.url('helpers/' + str(self.post.id) + '/')
        response = self.client.get(url)
        url = response.data['next']
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), REST_FRAMEWORK['PAGE_SIZE'])
        self.assertNotEqual(response.data['previous'], None)
        self.assertEqual(response.data['next'], None)
