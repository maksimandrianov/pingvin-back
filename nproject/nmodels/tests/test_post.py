from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from nmodels.models import Post
from nmodels.models import ConditionType, PostType
from nmodels.serializers import SimplePostsSerializer, EventPostsSerializer
from nmodels import lib
from nproject.settings import REST_FRAMEWORK
from nmodels.tests.lib import Creator, Request


class BasicTestCasePostCommon(TestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.post = Creator.create_post(self.user)

    def _post(self):
        return Creator.get_post(self.post.pk)

    def test_control(self):
        self.assertEqual(Post.objects.count(), 1)

    def test_filed_simple_equal(self):
        self.assertEqual(self.post.created_user.id, self.user.id)
        self.assertEqual(self.post.text, 'This is test post text')
        self.assertEqual(self.post.header, 'This is header')

    def test_filed_simple_additionally(self):
        self.post.req_helpers = 5
        self.post.helpers = 5
        self.post.views = 100
        self.post.save()
        post = self._post()
        self.assertEqual(post.req_helpers, 5)
        self.assertEqual(post.helpers, 5)
        self.assertEqual(post.views, 100)
        self.assertEqual(post.like, 0)
        self.assertEqual(post.dislike, 0)

    def test_filed_is_none(self):
        self.assertEqual(self.post.category is None, True)
        self.assertEqual(self.post.location is None, True)

    def test_field_category(self):
        c1 = Creator.create_category(1, 1)
        self.post.category = c1
        self.post.save()
        post = self._post()
        self.assertNotEqual(post.category, None)
        self.assertEqual(post.category.category_type, 1)
        self.assertEqual(post.category.subcategory_type, 1)

    def test_field_condition(self):
        c1 = Creator.create_condition(ConditionType.THANKS)
        self.post.condition = c1
        self.post.save()
        post = self._post()
        self.assertNotEqual(post.condition, None)
        self.assertEqual(post.condition.type, ConditionType.THANKS)

    def test_field_update_time(self):
        datetime_before = self.post.updated
        self.post.save()
        self.assertGreater(self.post.updated, datetime_before)

    def test_method_update_helpers(self):
        last_helpers = self.post.helpers
        self.post.inc_helpers()
        post = self._post()
        self.assertGreater(post.helpers, last_helpers)

    def test_method_moderation_over(self):
        post = Creator.create_event(self.user)

        self.assertEqual(post.type, PostType.EVENT)
        self.assertEqual(post.is_moderated, False)
        self.assertEqual(post.moderated_time, None)

        post.moderation_over()

        self.assertEqual(post.is_moderated, True)
        self.assertEqual(post.moderated_time is None, False)


class BasicTestCasePostsSerializer(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.serializer = SimplePostsSerializer
        super(TestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(TestCase, cls).tearDownClass()

    def setUp(self):
        self.user = Creator.create_user()
        self.post = Creator.create_post(self.user)

    def test_filed_simple_equal(self):
        request = Request(self.user, {})
        serializer = self.serializer(self.post, context={'request': request})
        data = serializer.data
        self.assertEqual(data.get('header'), 'This is header')
        self.assertEqual(data.get('text'), 'This is test post text')
        self.assertEqual(data.get('helpers'), 0)
        self.assertEqual(data.get('is_help'), False)
        self.assertEqual(data.get('is_like'), None)
        self.assertEqual(data.get('like'), 0)
        self.assertEqual(data.get('dislike'), 0)
        self.assertEqual(data.get('created_user').get('id'), self.user.id)
        self.assertEqual(data.get('created_user').get('first_name'), 'Maksim')
        self.assertEqual(data.get('created_user').get('second_name'), 'Andrianov')
        self.assertNotEqual(data.get('created'), None)
        # self.assertNotEqual(data.get('updated'), None)

    def test_method_create(self):
        data = {
            'text': 'Test text',
            'header': 'Test header',
            'category': Creator.create_category_dict(1, 1),
            'condition': Creator.create_condition_dict(ConditionType.MONEY, 100),
            'h_style': {
                'img': 'https://www.google.ru',
                'b_color': '#000000'
            }
        }
        request = Request(self.user, data)
        serializer = self.serializer(data=data, context={'request': request})
        if serializer.is_valid():
            serializer.create(serializer.validated_data)
        else:
            print(serializer.error_messages)

        post = Post.objects.get(text='Test text')

        self.assertEqual(post.text, 'Test text')
        self.assertEqual(post.header, 'Test header')
        self.assertEqual(post.category.category_type, 1)
        self.assertEqual(post.category.subcategory_type, 1)
        self.assertEqual(post.condition.type, ConditionType.MONEY)
        self.assertEqual(post.condition.money, 100)
        self.assertEqual(post.h_style.img, 'https://www.google.ru')
        self.assertEqual(post.h_style.b_color, '#000000')

    def test_method_after_like(self):
        self.user.like_post(self.post.id)
        request = Request(self.user, {})
        serializer = self.serializer(self.post, context={'request': request})
        data = serializer.data
        self.assertEqual(data.get('is_like'), True)

    def test_method_after_dislike(self):
        self.user.dislike_post(self.post.id)
        request = Request(self.user, {})
        serializer = self.serializer(self.post, context={'request': request})
        data = serializer.data
        self.assertEqual(data.get('is_like'), False)


class BasicTestCaseEventsSerializer(BasicTestCasePostsSerializer):

    @classmethod
    def setUpClass(cls):
        cls.serializer = EventPostsSerializer
        super(TestCase, cls).setUpClass()

    def setUp(self):
        self.user = Creator.create_user()
        self.post = Creator.create_event(self.user)

    def test_create_and_type(self):
        data = {
            'text': 'Test text',
            'header': 'Test header'
        }
        request = Request(self.user, data)
        serializer = self.serializer(data=data, context={'request': request})
        if serializer.is_valid():
            serializer.create(serializer.validated_data)
        else:
            print(serializer.error_messages)

        post = Post.objects.get(text='Test text')
        self.assertEqual(post.type, PostType.EVENT)
        self.assertEqual(post.is_moderated, False)
        self.assertEqual(post.moderated_time, None)


class BasicTestCasePostView(APITestCase):

    @classmethod
    def setUpClass(cls):
        cls.m_url = 'posts/'
        cls.o_url = 'post/'
        super(APITestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(APITestCase, cls).tearDownClass()

    def setUp(self):
        self.user = Creator.create_user()
        self.posts = Creator.create_posts(2, self.user)
        Creator.create_events(2, self.user)
        self.client.force_authenticate(user=self.user)

    def _moderate(self, id):
        pass

    def test_posts(self):
        url = lib.url(self.m_url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), REST_FRAMEWORK['PAGE_SIZE'])
        self.assertNotEqual(response.data['next'], None)
        self.assertEqual(response.data['previous'], None)

    def test_posts_next(self):
        url = lib.url(self.m_url)
        response = self.client.get(url)
        url = response.data['next']
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), REST_FRAMEWORK['PAGE_SIZE'])
        self.assertNotEqual(response.data['previous'], None)
        self.assertEqual(response.data['next'], None)

    def test_post_detail(self):
        url = lib.url(self.o_url + str(self.posts[0].id) + '/')
        response = self.client.get(url)
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data['header'], 'This is header')
        self.assertEqual(data['text'], 'This is test post text')
        self.assertEqual(data['helpers'], 0)
        self.assertEqual(data['is_help'], False)
        self.assertEqual(data['is_like'], None)
        self.assertEqual(data['like'], 0)
        self.assertEqual(data['dislike'], 0)
        self.assertEqual(data['created_user']['id'], self.user.id)
        self.assertEqual(data['created_user']['first_name'], 'Maksim')
        self.assertEqual(data['created_user']['second_name'], 'Andrianov')
        self.assertNotEqual(data['created'], None)
        # self.assertNotEqual(data['updated'], None)

    def test_post_create_min(self):
        url = lib.url(self.m_url)
        data = {
            'text': 'Test text',
            'header': 'Test header',

        }
        response = self.client.post(url, data=data, format='json')
        post = response.data
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(post['text'], 'Test text')
        self.assertEqual(post['header'], 'Test header')

    def test_post_create(self):
        url = lib.url(self.m_url)
        data = {
            'text': 'Test text',
            'header': 'Test header',
            'category': Creator.create_category_dict(1, 1),
            'condition': Creator.create_condition_dict(ConditionType.MONEY, 100),
            'h_style': {
                'img': 'https://www.google.ru',
                'b_color': '#000000'
            }
        }
        response = self.client.post(url, data=data, format='json')
        post = response.data

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(post['text'], 'Test text')
        self.assertEqual(post['header'], 'Test header')

        self._moderate(post['id'])

        durl = lib.url(self.o_url + str(post['id']) + '/')
        dresponse = self.client.get(durl)
        dpost = dresponse.data

        self.assertEqual(dpost['category']['category_type'], 1)
        self.assertEqual(dpost['category']['subcategory_type'], 1)
        self.assertEqual(dpost['condition']['type'], ConditionType.MONEY)
        self.assertEqual(dpost['condition']['money'], 100)
        self.assertEqual(dpost['h_style']['img'], 'https://www.google.ru')
        self.assertEqual(dpost['h_style']['b_color'], '#000000')

    def test_post_create_location(self):
        url = lib.url(self.m_url)
        data = {
            'text': 'Test text',
            'header': 'Test header',
            'location': {
                'country': 'Россия',
                'city': 'Санкт-Петербург',
                'address': 'Россия, Санкт-Петербург, пр. Энгельса 7',
                'point':  {
                    'latitude': 1.1,
                    'longitude': 1.1
                }
            }

        }
        response = self.client.post(url, data=data, format='json')
        post = response.data
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(post['text'], 'Test text')
        self.assertEqual(post['header'], 'Test header')

        self._moderate(post['id'])

        _url = lib.url(self.o_url + str(post['id']) + '/')
        _response = self.client.get(_url)
        _data = _response.data

        self.assertEqual(_data['location']['country'], 'Россия')
        self.assertEqual(_data['location']['city'], 'Санкт-Петербург')
        self.assertEqual(_data['location']['address'], 'Россия, Санкт-Петербург, пр. Энгельса 7')
        self.assertEqual(float(_data['location']['point']['latitude']), 1.1)
        self.assertEqual(float(_data['location']['point']['longitude']), 1.1)


class BasicEventCasePostView(BasicTestCasePostView):

    @classmethod
    def setUpClass(cls):
        cls.m_url = 'events/'
        cls.o_url = 'event/'
        super(APITestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(APITestCase, cls).tearDownClass()

    def setUp(self):
        self.user = Creator.create_user()
        self.posts = Creator.create_events(2, self.user)
        for p in self.posts:
            p.moderation_over()
        Creator.create_posts(2, self.user)
        self.client.force_authenticate(user=self.user)

    def _moderate(self, id):
        Post.objects.get(id=id).moderation_over()