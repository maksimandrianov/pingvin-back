# from sphinxql.query import QuerySet
# from tests import SphinxQLTestCase
# from nmodels.indexes import PostIndex, UserIndex
# from nmodels.models import Post, User, Condition, Category, ConditionType
# from nmodels.tests.lib import Creator
#
# # todo: test post search
#
#
# class BasicTestCaseSearch(SphinxQLTestCase):
#
#     def setUp(self):
#         self.user = Creator.create_user()
#
#         self.query_post = QuerySet(PostIndex)
#         self.query_user = QuerySet(UserIndex)
#
#         super(self.__class__, self).setUp()
#
#     def test_search_post(self):
#         post = Creator.create_post(self.user)
#         self.index()
#         q = self.query_post.search('@text This is test post text')
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, post.id)
#
#     def test_search_post_ru(self):
#         post_ru = Post.objects.create(text='Тест на русском',
#                                       header='Заголовок',
#                                       created_user=self.user)
#         self.index()
#         q = self.query_post.search('@text Тест на русском')
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, post_ru.id)
#
#     def test_search_user_full_name(self):
#         user = User.objects.create(email='maksim@yo.ru', password='***',
#                                    first_name='Vasia',
#                                    second_name='Malal')
#         self.index()
#         q = self.query_user.search('@full_name Vasia Malal')
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, user.id)
#
#     def test_search_user_location(self):
#         user = User.objects.create(email='maksim@yo.ru', password='***',
#                                    first_name='Vasia',
#                                    second_name='Malal')
#         user.settings.location.address = 'Россия, Санкт Петербург'
#         user.settings.location.save()
#
#         self.index()
#         q = self.query_user.search('@location Россия')
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, user.id)
#
#
# class BasicTestCaseSearchUserModel(SphinxQLTestCase):
#
#     def setUp(self):
#         self.user1 = User.objects.create(email='maksim@yo.ru', password='***',
#                                          first_name='Максим',
#                                          second_name='Банановчи')
#         self.user1.settings.location.address = 'Россия, Санкт Петербург'
#         self.user1.settings.location.save()
#
#         self.user2 = User.objects.create(email='serg@yo.ru', password='***',
#                                          first_name='Sergey',
#                                          second_name='Test')
#         self.user2.settings.location.address = 'Россия, Москва'
#         self.user2.settings.location.save()
#
#         super(self.__class__, self).setUp()
#
#     def test_search_with_name(self):
#         params = {'name': 'Sergey Test'}
#         q = User.search(params)
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, self.user2.id)
#
#     def test_search_with_city(self):
#         params = {'city': 'Санкт Петербург'}
#         q = User.search(params)
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, self.user1.id)
#
#     def test_search_with_country(self):
#         params = {'country': 'Россия'}
#         q = User.search(params)
#
#         self.assertEqual(len(q), 2)
#
#
# class BasicTestCaseSearchPostModel(SphinxQLTestCase):
#
#     def setUp(self):
#         user = Creator.create_user()
#
#         cond1 = Creator.create_condition(ConditionType.MONEY, 100)
#         cond2 = Creator.create_condition(ConditionType.OTHER, 0, 'Test condition')
#
#         cat1 = Creator.create_category(1, 1)
#         cat2 = Creator.create_category(1, 2)
#
#         self.post1 = Post.objects.create(text='One is test post text', header='This is header',
#                                          created_user=user, condition=cond1, category=cat1)
#         self.post2 = Post.objects.create(text='Two is test post text', header='This is header',
#                                          created_user=user, condition=cond2, category=cat2)
#         super(self.__class__, self).setUp()
#
#     def test_search_condition_with_type(self):
#         params = {'condition': {
#             'type': ConditionType.MONEY
#         }}
#         q = Post.search(params)
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, self.post1.id)
#
#     def test_search_condition_with_type_and_money(self):
#         params = {'condition': {
#             'type': ConditionType.MONEY,
#             'money': 100
#         }}
#         q = Post.search(params)
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, self.post1.id)
#
#     def test_search_condition_with_type_and_other(self):
#         params = {'condition': {
#             'type': ConditionType.OTHER,
#             'other': 'condition'
#         }}
#         q = Post.search(params)
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, self.post2.id)
#
#     def test_search_category_with_category_type(self):
#         params = {'category': {
#             'category_type': 1
#         }}
#         q = Post.search(params)
#
#         self.assertEqual(len(q), 2)
#
#     def test_search_category_with_category_type_and_subcategory_type(self):
#         params = {'category': {
#             'category_type': 1,
#             'subcategory_type': 1
#         }}
#         q = Post.search(params)
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, self.post1.id)
#
#     def test_search_with_search(self):
#         params = {'search': 'One'}
#         q = Post.search(params)
#
#         self.assertEqual(len(q), 1)
#         self.assertEqual(q[0].id, self.post1.id)