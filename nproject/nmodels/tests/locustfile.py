from locust import HttpLocust, TaskSet, task
import json
from collections import namedtuple

EMAIL = 'alisa101@gmail.com'
PASSWORD = 'alisa101@gmail.com'


def _json_object_hook(d):
    return namedtuple('X', d.keys())(*d.values())


def json2obj(data):
    return json.loads(data, object_hook=_json_object_hook)


class UserBehavior(TaskSet):

    def __init__(self, parent):
        self._token = ''
        super(self.__class__, self).__init__(parent)

    def on_start(self):
        self._token = self.login()

    def login(self):
        req = self.client.post('/auth/login/', {'email': EMAIL, 'password': PASSWORD})
        return json2obj(req.text).auth_token

    @task(1)
    def users(self):
        self.client.get('/users/', headers=self.token)

    @task(1)
    def posts(self):
        self.client.get('/posts/', headers=self.token)

    @task(1)
    def blog_posts(self):
        self.client.get('/blog-posts/', headers=self.token)

    @task(1)
    def events(self):
        self.client.get('/events/', headers=self.token)

    @task(1)
    def replies(self):
        self.client.get('/replies/', headers=self.token)

    @task(1)
    def post(self):
        self.client.get('/post/11/', headers=self.token)

    @property
    def token(self):
        return {'Authorization': 'Token ' + self._token}


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 0
    max_wait = 10




