from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from nmodels.models import UserSettings, Category, ConditionType
from nmodels.serializers import UserSettingsSerializer
from nmodels import lib
from nmodels.tests.lib import Creator, Request
from django.contrib.gis.geos import Point


class BasicTestCaseUserSettingsCommon(TestCase):

    def setUp(self):
        self.user = Creator.create_user()

    def test_control(self):
        self.assertEqual(UserSettings.objects.count(), 1)

    def test_filed_simple_equal(self):
        settings = self.user.settings
        self.assertEqual(settings.categories.count(), 0)
        self.assertEqual(settings.contacts.count(), 0)
        self.assertEqual(settings.location.country, '')
        self.assertEqual(settings.location.city, '')
        self.assertEqual(settings.location.address, '')
        self.assertEqual(settings.location.point, Point(0.0, 0.0))

    def test_filed_is_not_none(self):
        self.assertNotEqual(self.user.settings.location, None)

    def test_field_categories_number(self):
        settings = self.user.settings
        c1 = Creator.create_category(1, 1)
        c2 = Creator.create_category(2, 2)
        settings.categories.add(*[c1, c2])
        self.assertEqual(settings.categories.count(), 2)

    def test_field_contacts_number(self):
        settings = self.user.settings
        c1 = Creator.create_contact(1, '+79817068020')
        c2 = Creator.create_contact(2, 'test@test.ru')
        settings.contacts.add(*[c1, c2])
        self.assertEqual(settings.contacts.count(), 2)

    def test_filed_simple_additionally_geo(self):
        settings = self.user.settings
        settings.location.country = 'Россия'
        settings.location.city = 'Санкт-Петербург'
        settings.location.address = 'Россия, Санкт-Петербург, пр. Энгельса 7'
        settings.location.point = Point(1.1, 1.1)
        settings.location.save()

        _settings = UserSettings.objects.get(id=self.user.id)
        self.assertEqual(_settings.location.country, 'Россия')
        self.assertEqual(_settings.location.city, 'Санкт-Петербург')
        self.assertEqual(_settings.location.address, 'Россия, Санкт-Петербург, пр. Энгельса 7')
        self.assertEqual(_settings.location.point, Point(1.1, 1.1))


class BasicTestCaseUserSettingsAddContacts(TestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.d_contacts = [
            Creator.create_contact_dict(1, '+79817068020'),
            Creator.create_contact_dict(2, 'test@test.ru'),
            Creator.create_contact_dict(3, 'https://vk.com/maxandr')
        ]

    def test_method_update_contacts_add_count(self):
        self.user._update_contacts(self.d_contacts)
        self.user.settings.update_contacts([i.id for i in self.user.contacts.all()])
        self.assertEqual(self.user.contacts.count(), 3)
        self.assertEqual(self.user.settings.contacts.count(), 3)

    def test_method_update_contacts_add_emty_data(self):
        self.user._update_contacts([])
        self.user.settings.update_contacts([])
        self.assertEqual(self.user.contacts.count(), 0)
        self.assertEqual(self.user.settings.contacts.count(), 0)

    def test_method_update_contacts_del_data(self):
        self.user._update_contacts(self.d_contacts)
        self.user.settings.update_contacts([i.id for i in self.user.contacts.all()])
        ct_old = self.user.contacts.order_by('id')
        d_contacts = self.d_contacts[0:2]
        self.user.update_contacts_sync(d_contacts)
        ct = self.user.contacts.order_by('id')
        user = Creator.get_user()
        self.assertEqual(user.contacts.count(), 2)
        self.assertEqual(user.settings.contacts.count(), 2)

    def test_method_update_contacts_insert_data(self):
        self.user._update_contacts(self.d_contacts)
        self.user.settings.update_contacts([i.id for i in self.user.contacts.all()])
        ct_old = self.user.contacts.order_by('id')
        d_contacts = list(self.d_contacts)
        d_contacts.append(Creator.create_contact_dict(4, 'https://fb.com/maxandr'))
        self.user.update_contacts_sync(d_contacts)
        ct = self.user.contacts.order_by('id')
        user = Creator.get_user()
        self.assertEqual(user.contacts.count(), 4)
        self.assertEqual(user.settings.contacts.count(), 3)

    def test_method_update_contacts_insert_repeated_data(self):
        self.user._update_contacts(self.d_contacts)
        self.user.settings.update_contacts([i.id for i in self.user.contacts.all()])
        self.user.update_contacts_sync(self.d_contacts)
        self.user.settings.update_contacts([i.id for i in self.user.contacts.all()])
        self.user.update_contacts_sync(self.d_contacts)
        user = Creator.get_user()
        self.assertEqual(user.contacts.count(), 3)
        self.assertEqual(user.settings.contacts.count(), 3)


class BasicTestCaseUserSettingsAddCategories(TestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.d_categories = [
            Creator.create_category_dict(1, 1),
            Creator.create_category_dict(2, 2),
            Creator.create_category_dict(3, 3)
        ]

    def test_method_update_categories_add_count(self):
        self.user._update_categories(self.d_categories)
        self.user.settings.update_categories([i.id for i in self.user.categories.all()])
        self.assertEqual(self.user.categories.count(), 3)
        self.assertEqual(self.user.settings.categories.count(), 3)

    def test_method_update_categories_add_emty_data(self):
        self.user._update_categories([])
        self.user.settings.update_categories([])
        self.assertEqual(self.user.categories.count(), 0)
        self.assertEqual(self.user.settings.categories.count(), 0)

    def test_method_update_categories_del_data(self):
        self.user._update_categories(self.d_categories)
        self.user.settings.update_categories([i.id for i in self.user.categories.all()])
        ct_old = self.user.categories.order_by('id')
        d_categories = self.d_categories[0:2]
        self.user.update_categories_sync(d_categories)
        ct = self.user.categories.order_by('id')
        user = Creator.get_user()
        self.assertEqual(user.categories.count(), 2)
        self.assertEqual(user.settings.categories.count(), 2)

    def test_method_update_categories_insert_data(self):
        self.user._update_categories(self.d_categories)
        self.user.settings.update_categories([i.id for i in self.user.categories.all()])
        ct_old = self.user.categories.order_by('id')
        d_categories = list(self.d_categories)
        d_categories.append(Creator.create_category_dict(4, 4))
        self.user.update_categories_sync(d_categories)
        ct = self.user.categories.order_by('id')
        user = Creator.get_user()
        self.assertEqual(user.categories.count(), 4)
        self.assertEqual(user.settings.categories.count(), 3)

    def test_method_update_categories_insert_repeated_data(self):
        self.user._update_categories(self.d_categories)
        self.user.settings.update_categories([i.id for i in self.user.categories.all()])
        self.user.update_categories_sync(self.d_categories)
        self.user.settings.update_categories([i.id for i in self.user.categories.all()])
        self.user.update_categories_sync(self.d_categories)
        user = Creator.get_user()
        self.assertEqual(user.categories.count(), 3)
        self.assertEqual(user.settings.categories.count(), 3)


class BasicTestCaseUserSettingsSerializer(TestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.d_categories = [
            Creator.create_category_dict(1, 1),
            Creator.create_category_dict(2, 2),
            Creator.create_category_dict(3, 3)
        ]
        self.d_contacts = [
            Creator.create_contact_dict(1, '+79817068020'),
            Creator.create_contact_dict(2, 'test@test.ru'),
            Creator.create_contact_dict(3, 'https://vk.com/maxandr')
        ]

    def test_filed_simple_equal(self):
        serializer = UserSettingsSerializer(self.user.settings)
        data = serializer.data
        self.assertEqual(data['categories'], [])
        self.assertEqual(data['contacts'], [])
        self.assertEqual(data['location']['country'], '')
        self.assertEqual(data['location']['city'], '')
        self.assertEqual(data['location']['address'], '')
        self.assertEqual(float(data['location']['point']['latitude']), 0.0)
        self.assertEqual(float(data['location']['point']['longitude']), 0.0)

    def test_method_update_simple_add(self):
        self.user._update_categories(self.d_categories)
        self.user._update_contacts(self.d_contacts)
        data = {
            'contacts_ids': [i.id for i in self.user.contacts.all()],
            'categories_ids': [i.id for i in self.user.categories.all()],
            'location': {
                'country': 'Россия',
                'city': 'Санкт-Петербург',
                'address': 'Россия, Санкт-Петербург, пр. Энгельса 7',
                 'point':  {
                     'latitude': 1.1,
                     'longitude': 1.1
                 }
            }
        }

        request = Request(self.user, {})
        serializer = UserSettingsSerializer(self.user.settings, data=data,
                                            partial=True, context={'request': request})
        if serializer.is_valid():
            serializer.save()
        else:
            print(serializer.error_messages)

        data = serializer.data
        _settings = UserSettings.objects.get(id=self.user.id)

        self.assertEqual(_settings.location.country, 'Россия')
        self.assertEqual(_settings.location.city, 'Санкт-Петербург')
        self.assertEqual(_settings.location.address, 'Россия, Санкт-Петербург, пр. Энгельса 7')
        self.assertEqual(_settings.location.point, Point(1.1, 1.1))

        self.assertEqual(len(data['categories']), 3)
        self.assertEqual(len(data['contacts']), 3)
        self.assertEqual(data['location']['country'], 'Россия')
        self.assertEqual(data['location']['city'], 'Санкт-Петербург')
        self.assertEqual(data['location']['address'], 'Россия, Санкт-Петербург, пр. Энгельса 7')
        self.assertEqual(float(data['location']['point']['latitude']), 1.1)
        self.assertEqual(float(data['location']['point']['longitude']), 1.1)


class BasicTestCaseUserSettingsView(APITestCase):

    def setUp(self):
        self.user = Creator.create_user()
        self.d_categories = [
            Creator.create_category_dict(1, 1),
            Creator.create_category_dict(2, 2),
            Creator.create_category_dict(3, 3)
        ]
        self.d_contacts = [
            Creator.create_contact_dict(1, '+79817068020'),
            Creator.create_contact_dict(2, 'test@test.ru'),
            Creator.create_contact_dict(3, 'https://vk.com/maxandr')
        ]

        self.vitya = Creator.create_user(3)
        self.post_user = Creator.create_post(self.user)
        self.client.force_authenticate(user=self.user)

    def _replies(self):
        url = lib.url('replies/')
        response = self.client.get(url)
        return response.status_code, response.data

    def test_user_settings_detail(self):
        url = lib.url('settings/' + str(self.user.id) + '/')
        response = self.client.get(url)
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data['categories'], [])
        self.assertEqual(data['contacts'], [])
        self.assertEqual(data['location']['country'], '')
        self.assertEqual(data['location']['city'], '')
        self.assertEqual(data['location']['address'], '')
        self.assertEqual(float(data['location']['point']['latitude']), 0.0)
        self.assertEqual(float(data['location']['point']['longitude']), 0.0)

    def test_user_settings_update(self):
        url = lib.url('settings/' + str(self.user.id) + '/')
        self.user._update_categories(self.d_categories)
        self.user._update_contacts(self.d_contacts)
        data = {
            'contacts_ids': [i.id for i in self.user.contacts.all()],
            'categories_ids': [i.id for i in self.user.categories.all()],
            'location': {
                'country': 'Россия',
                'city': 'Санкт-Петербург',
                'address': 'Россия, Санкт-Петербург, пр. Энгельса 7',
                 'point':  {
                     'latitude': 1.1,
                     'longitude': 1.1
                 }
            }
        }
        response = self.client.patch(url, data=data, format='json')
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(data['categories']), 3)
        self.assertEqual(len(data['contacts']), 3)
        self.assertEqual(data['location']['country'], 'Россия')
        self.assertEqual(data['location']['city'], 'Санкт-Петербург')
        self.assertEqual(data['location']['address'], 'Россия, Санкт-Петербург, пр. Энгельса 7')
        self.assertEqual(float(data['location']['point']['latitude']), 1.1)
        self.assertEqual(float(data['location']['point']['longitude']), 1.1)

    # todo: more test test_category_filter_applay
    def test_category_filter_applay(self):
        url = lib.url('settings/' + str(self.user.id) + '/')
        self.user._update_categories(self.d_categories)
        self.user._update_contacts(self.d_contacts)
        data = {
            'categories_ids': [i.id for i in self.user.categories.all().order_by('category_type')[:1]]
        }
        self.client.patch(url, data=data, format='json')

        p1 = Creator.create_post(self.user)
        p2 = Creator.create_post(self.user)
        p3 = Creator.create_post(self.user)
        p4 = Creator.create_post(self.user)

        c1, created1 = Category.objects.get_or_create(category_type=1, subcategory_type=1)
        c2, created2 = Category.objects.get_or_create(category_type=2, subcategory_type=2)
        c3, created3 = Category.objects.get_or_create(category_type=3, subcategory_type=3)

        p1.category = c1
        p2.category = c2
        p3.category = c3

        p1.save()
        p2.save()
        p3.save()

        _url = lib.url('posts/')
        response = self.client.get(_url, format='json')

        data = response.data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(data['results']), 1)

    # todo: more test test_user_public_contacts
    def test_user_public_contacts(self):
        url = lib.url('settings/' + str(self.user.id) + '/')
        self.user._update_contacts(self.d_contacts)
        data = {
            'contacts_ids': [i.id for i in self.user.contacts.all()[:1]],
        }
        response = self.client.patch(url, data=data, format='json')
        data = response.data

        self.client.force_authenticate(user=self.vitya)
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_user.id, cont)

        self.client.force_authenticate(user=self.user)
        self.user.select_helper(self.post_user.id, self.vitya.id)

        self.client.force_authenticate(user=self.vitya)

        status_code2, data2 = self._replies()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(status_code2, status.HTTP_200_OK)
        self.assertEqual(len(data2['results'][0]['public_contacts']), 1)


