from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from nmodels.models import Post, User
from nmodels.models import ConditionType, StatusType, Help
from nmodels.serializers import SimplePostsSerializer
from nmodels import lib
from nproject.settings import REST_FRAMEWORK
from nmodels.tests.lib import Creator, Request
import time


class BasicTestCaseRepliesBasic(TestCase):

    def setUp(self):
        self.maksim = Creator.create_user(2)
        self.vitya = Creator.create_user(3)
        self.andrey = Creator.create_user(4)
        self.post_maksim = Creator.create_post(self.maksim)

    def test_new_helper(self):
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)

        replies1 = list(self.maksim._replies_to_posts_q())
        replies2 = list(self.maksim._replies_to_help_q())
        replies3 = list(self.vitya._replies_to_posts_q())
        replies4 = list(self.vitya._replies_to_help_q())

        self.assertEqual(len(replies1), 1)
        self.assertEqual(len(replies2), 0)
        self.assertEqual(len(replies3), 0)
        self.assertEqual(len(replies4), 0)

        self.assertEqual(replies1[0].user.id, self.vitya.id)
        self.assertEqual(replies1[0].post.id, self.post_maksim.id)
        self.assertEqual(replies1[0].status, StatusType.NEW_HELPER)

    def test_update_condition_and_new_helper(self):
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        h = self.vitya.help(self.post_maksim.id, cont)
        h1 = self.andrey.help(self.post_maksim.id, cont)

        cont_new = Creator.create_condition_dict(ConditionType.MONEY, 200)
        self.vitya.update_condition_help(self.post_maksim.id, cont_new)

        replies1 = list(self.maksim._replies_to_posts_q())
        replies2 = list(self.maksim._replies_to_help_q())
        replies3 = list(self.vitya._replies_to_posts_q())
        replies4 = list(self.vitya._replies_to_help_q())
        replies5 = list(self.andrey._replies_to_posts_q())
        replies6 = list(self.andrey._replies_to_help_q())

        self.assertEqual(len(replies1), 2)
        self.assertEqual(len(replies2), 0)
        self.assertEqual(len(replies3), 0)
        self.assertEqual(len(replies4), 0)
        self.assertEqual(len(replies5), 0)
        self.assertEqual(len(replies6), 0)

        self.assertEqual(replies1[1].user.id, self.vitya.id)
        self.assertEqual(replies1[1].post_id, self.post_maksim.id)

        self.assertEqual(replies1[0].user_id, self.andrey.id)
        self.assertEqual(replies1[0].post_id, self.post_maksim.id)

        self.assertEqual(replies1[1].status, StatusType.UPDATE_CONDITION)
        self.assertEqual(replies1[0].status, StatusType.NEW_HELPER)

    def test_select_helper(self):
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)
        self.maksim.select_helper(self.post_maksim.id, self.vitya.id)

        replies1 = list(self.maksim._replies_to_posts_q())
        replies2 = list(self.maksim._replies_to_help_q())
        replies3 = list(self.vitya._replies_to_posts_q())
        replies4 = list(self.vitya._replies_to_help_q())

        self.assertEqual(len(replies1), 0)
        self.assertEqual(len(replies2), 0)
        self.assertEqual(len(replies3), 0)
        self.assertEqual(len(replies4), 1)

        self.assertEqual(replies4[0].user_id, self.vitya.id)
        self.assertEqual(replies4[0].post_id, self.post_maksim.id)

        self.assertEqual(replies4[0].status, StatusType.SELECT_HELPER)

    def test_cancel_helper(self):
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)
        self.maksim.select_helper(self.post_maksim.id, self.vitya.id)
        self.maksim.cancel_helper(self.post_maksim.id, self.vitya.id)

        replies1 = list(self.maksim._replies_to_posts_q())
        replies2 = list(self.maksim._replies_to_help_q())
        replies3 = list(self.vitya._replies_to_posts_q())
        replies4 = list(self.vitya._replies_to_help_q())

        self.assertEqual(len(replies1), 0)
        self.assertEqual(len(replies2), 0)
        self.assertEqual(len(replies3), 0)
        self.assertEqual(len(replies4), 1)

        self.assertEqual(replies4[0].user_id, self.vitya.id)
        self.assertEqual(replies4[0].post_id, self.post_maksim.id)

        self.assertEqual(replies4[0].status, StatusType.CANCEL_HELPER)

    def test_finish_help(self):
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)
        self.maksim.select_helper(self.post_maksim.id, self.vitya.id)
        self.maksim.select_successfully(self.post_maksim.id, self.vitya.id)

        replies1 = list(self.maksim._replies_to_posts_q())
        replies2 = list(self.maksim._replies_to_help_q())
        replies3 = list(self.vitya._replies_to_posts_q())
        replies4 = list(self.vitya._replies_to_help_q())

        self.assertEqual(len(replies1), 0)
        self.assertEqual(len(replies2), 0)
        self.assertEqual(len(replies3), 0)
        self.assertEqual(len(replies4), 1)

        self.assertEqual(replies4[0].user_id, self.vitya.id)
        self.assertEqual(replies4[0].post_id, self.post_maksim.id)

        self.assertEqual(replies4[0].status, StatusType.FINIS_HELP)

    def test_method_count_replies_simple(self):
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)
        self.andrey.help(self.post_maksim.id, cont)
        cont_new = Creator.create_condition_dict(ConditionType.MONEY, 200)
        self.vitya.update_condition_help(self.post_maksim.id, cont_new)

        self.assertEqual(self.maksim.count_new_replies(), 2)

    def test_method_count_replies(self):
        old_old_count_replies = self.maksim.count_new_replies()

        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)

        old_count_replies = self.maksim.count_new_replies()

        self.andrey.help(self.post_maksim.id, cont)
        self.maksim.see_replies()
        cont_new = Creator.create_condition_dict(ConditionType.MONEY, 200)
        self.vitya.update_condition_help(self.post_maksim.id, cont_new)

        self.assertEqual(old_old_count_replies, 0)
        self.assertEqual(old_count_replies, 1)
        self.assertEqual(self.maksim.count_new_replies(), 1)


class BasicTestCaseRepliesBasicView(APITestCase):

    def setUp(self):
        self.maksim = Creator.create_user(2)
        self.vitya = Creator.create_user(3)
        self.andrey = Creator.create_user(4)
        self.post_maksim = Creator.create_post(self.maksim)

    def _replies(self):
        url = lib.url('replies/')
        response = self.client.get(url)
        return response.status_code, response.data

    def _replies_count(self):
        url = lib.url('replies/count/')
        response = self.client.get(url)
        return response.status_code, response.data

    def _assert(self, data, status):
        self.assertEqual(len(data['results']), 1)
        self.assertEqual(data['results'][0]['status'], status)
        self.assertEqual(data['results'][0]['user']['id'], self.vitya.id)
        self.assertEqual(data['results'][0]['post']['id'], self.post_maksim.id)
        self.assertEqual(len(data['results'][0]['helpers']), 1)
        self.assertEqual(data['results'][0]['helpers'][0]['user']['id'], self.vitya.id)

    def test_new_helper(self):
        self.client.force_authenticate(user=self.maksim)
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)

        status_code, data = self._replies()

        self.assertEqual(status_code, status.HTTP_200_OK)
        self._assert(data, StatusType.NEW_HELPER)

    def test_update_condition_and_new_helper(self):
        self.client.force_authenticate(user=self.maksim)
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)
        self.andrey.help(self.post_maksim.id, cont)

        cont_new = Creator.create_condition_dict(ConditionType.MONEY, 200)
        self.vitya.update_condition_help(self.post_maksim.id, cont_new)

        status_code, data = self._replies()

        self.assertEqual(status_code, status.HTTP_200_OK)

        self.assertEqual(len(data['results']), 2)
        self.assertEqual(data['results'][1]['status'], StatusType.UPDATE_CONDITION)
        self.assertEqual(data['results'][1]['user']['id'], self.vitya.id)
        self.assertEqual(data['results'][1]['post']['id'], self.post_maksim.id)
        self.assertEqual(data['results'][0]['status'], StatusType.NEW_HELPER)
        self.assertEqual(data['results'][0]['user']['id'], self.andrey.id)
        self.assertEqual(data['results'][0]['post']['id'], self.post_maksim.id)
        self.assertEqual(len(data['results'][0]['helpers']), 2)
        # self.assertEqual(data['results'][0]['helpers'][0]['user']['id'], self.vitya.id)
        # self.assertEqual(data['results'][0]['helpers'][1]['user']['id'], self.andrey.id)

    def test_select_helper(self):
        self.client.force_authenticate(user=self.vitya)
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)
        self.maksim.select_helper(self.post_maksim.id, self.vitya.id)

        status_code, data = self._replies()

        self.assertEqual(status_code, status.HTTP_200_OK)
        self._assert(data, StatusType.SELECT_HELPER)

    def test_cancel_helper(self):
        self.client.force_authenticate(user=self.vitya)
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)
        self.maksim.select_helper(self.post_maksim.id, self.vitya.id)
        self.maksim.cancel_helper(self.post_maksim.id, self.vitya.id)

        status_code, data = self._replies()

        self.assertEqual(status_code, status.HTTP_200_OK)
        self._assert(data, StatusType.CANCEL_HELPER)

    def test_finish_help(self):
        self.client.force_authenticate(user=self.vitya)
        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)
        self.maksim.select_helper(self.post_maksim.id, self.vitya.id)
        self.maksim.select_successfully(self.post_maksim.id, self.vitya.id)

        status_code, data = self._replies()

        self.assertEqual(status_code, status.HTTP_200_OK)
        self._assert(data, StatusType.FINIS_HELP)

    def test_method_count_replies(self):
        self.client.force_authenticate(user=self.maksim)
        status_code, data = self._replies_count()
        self.assertEqual(status_code, status.HTTP_200_OK)
        self.assertEqual(data['count'], 0)

        cont = Creator.create_condition_dict(ConditionType.MONEY, 100)
        self.vitya.help(self.post_maksim.id, cont)

        status_code, data = self._replies_count()
        self.assertEqual(status_code, status.HTTP_200_OK)
        self.assertEqual(data['count'], 1)

        self.andrey.help(self.post_maksim.id, cont)
        self._replies()
        cont_new = Creator.create_condition_dict(ConditionType.MONEY, 200)
        self.vitya.update_condition_help(self.post_maksim.id, cont_new)

        status_code, data = self._replies_count()
        self.assertEqual(status_code, status.HTTP_200_OK)
        self.assertEqual(data['count'], 1)
