from nmodels.models import User, Post, Category, Contact, Condition
from nmodels.models import CommentUser, CommentPost, PostType
from nproject.settings import REST_FRAMEWORK
import time
from datetime import date


def ssleep(t):
    time.sleep(t)
    return True


class Creator():
    @staticmethod
    def create_user(add_to_email=None):
        email = 'maksim@gmail.com'
        email = email if add_to_email is None else email + str(add_to_email)
        return User.objects.get_or_create(email=email, password='***', first_name='Maksim',
                                          second_name='Andrianov')

    @staticmethod
    def create_post(created_user, location=None):
        return Post.objects.create(text='This is test post text',
                                   header='This is header', created_user=created_user, location=location)

    @staticmethod
    def create_event(created_user, location=None):
        return Post.objects.create(text='This is test post text', type=PostType.EVENT,
                                   header='This is header', created_user=created_user, location=location)

    @staticmethod
    def create_comment_post(created_user, post):
        return CommentPost.objects.create(text='This is test post comment text',
                                          created_user=created_user, post=post)

    @staticmethod
    def create_comment_user(created_user, user):
        return CommentUser.objects.create(text='This is test user comment text',
                                          created_user=created_user, user=user)

    @staticmethod
    def get_user():
        return User.objects.get(email='maksim@gmail.com')

    @staticmethod
    def get_post(id):
        return Post.objects.get(pk=id)

    @staticmethod
    def get_comment_post(id):
        return CommentPost.objects.get(pk=id)

    @staticmethod
    def get_comment_user(id):
        return CommentUser.objects.get(pk=id)

    @staticmethod
    def create_category(ct, st):
        return Category.objects.create(category_type=ct, subcategory_type=st)

    @staticmethod
    def create_contact(t, d):
        return Contact.objects.create(type=t, data=d)

    @staticmethod
    def create_condition(t, m=0, o=''):
        return Condition.objects.create(type=t, money=m, other=o)

    @staticmethod
    def create_contact_dict(t, d):
        return {'type': t, 'data': d}

    @staticmethod
    def create_category_dict(ct, st):
        return {'category_type': ct, 'subcategory_type': st}

    @staticmethod
    def create_condition_dict(t, m=0, o=''):
        return {'type': t, 'money': m, 'other': o}

    @staticmethod
    def create_users(page):
        return [Creator.create_user(i) for i in range(page*REST_FRAMEWORK['PAGE_SIZE'])]

    @staticmethod
    def create_posts(page, user=None, t=0):
        created_user = Creator.create_user() if user is None else user
        return [Creator.create_post(created_user=created_user) for i in range(page*REST_FRAMEWORK['PAGE_SIZE'])
                if ssleep(t)]

    @staticmethod
    def create_events(page, user=None, t=0):
        created_user = Creator.create_user() if user is None else user
        return [Creator.create_event(created_user=created_user) for i in
                range(page*REST_FRAMEWORK['PAGE_SIZE']) if ssleep(t)]

    @staticmethod
    def create_posts_geo(geos, user=None):
        created_user = Creator.create_user() if user is None else user
        return [Creator.create_post(created_user=created_user, location=geos[i]) for i in range(len(geos))]

    @staticmethod
    def create_comments_user(page, user=None, created_user=None):
        user = Creator.create_user(11) if user is None else user
        created_user = Creator.create_user(22) if created_user is None else created_user
        return [Creator.create_comment_user(created_user=created_user, user=user)
                for i in range(page*REST_FRAMEWORK['PAGE_SIZE'])]

    @staticmethod
    def create_comments_post(page, post=None, created_user=None):
        created_user = Creator.create_user(22) if created_user is None else created_user
        post = Creator.create_post(created_user=created_user) if post is None else post
        return [Creator.create_comment_post(created_user=created_user, post=post)
                for i in range(page*REST_FRAMEWORK['PAGE_SIZE'])]

    @staticmethod
    def create_user_max(add_to_email=None):
        email = 'maksim@gmail.com'
        email = email if add_to_email is None else email + str(add_to_email)
        categories = [
            Creator.create_category(1, 1),
            Creator.create_category(2, 2),
            Creator.create_category(3, 3)
        ]
        contacts = [
            Creator.create_contact(1, '+79817068020'),
            Creator.create_contact(2, 'test@test.ru'),
            Creator.create_contact(3, 'https://vk.com/maxandr')
        ]
        user = User.objects.create(email=email, password='***', first_name='Maksim',
                                   second_name='Andrianov', bday=date(2000, 2, 2),
                                   info='This is info rundom This is info rundom This is info rundom This is info rundom '
                                        'This is info rundom This is info rundom This is info rundom This is info rundom '
                                        'This is info rundom This is info rundom This is info rundom This is info rundom ',
                                   gender='m')
        user.contacts.add(*contacts)
        user.categories.add(*categories)
        return user


    @staticmethod
    def create_users_max(page):
        return [Creator.create_user_max(i) for i in range(page*REST_FRAMEWORK['PAGE_SIZE'])]


class Request:
    def __init__(self, user, data, method=None):
        self.user = user
        self.data = data
        self.method = method


class View:
    def __init__(self, kwargs):
        self.kwargs = kwargs