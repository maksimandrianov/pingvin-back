from django.db.models.functions import Concat, Value
from django.db.models import F, CharField
from sphinxql.manager import IndexManager
from sphinxql import indexes, fields
from nmodels import models


class PostManager(IndexManager):
    def get_queryset(self):
        return models.Post.actually_q()


class PostIndex(indexes.Index):
    header = fields.Text(model_attr='header')
    text = fields.Text(model_attr='text')
    full = fields.IndexedString(model_attr=Concat('header', Value(' '),
                                                  'text', output_field=CharField()))

    type = fields.Integer(model_attr='type')

    condition_type = fields.Integer(model_attr='condition__type')
    condition_money = fields.Integer(model_attr='condition__money')

    category_category_type = fields.Integer(model_attr='category__category_type')
    category_subcategory_type = fields.Integer(model_attr='category__subcategory_type')

    other_condition = fields.IndexedString(model_attr='condition__other')

    objects = PostManager()

    class Meta:
        model = models.Post


class UserIndex(indexes.Index):
    first_name = fields.Text(model_attr='first_name')
    second_name = fields.Text(model_attr='second_name')
    full_name = fields.IndexedString(model_attr=Concat('first_name', Value(' '),
                                                       'second_name', output_field=CharField()))
    location = fields.IndexedString(model_attr='settings__location__address')

    class Meta:
        model = models.User