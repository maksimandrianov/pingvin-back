from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from nmodels import views

REGEX_ID = r'[0-9]{1,12}'
COMMENT_PARAMS = {'get': 'list', 'post': 'create'}
HELP_PARAMS = {'get': 'list'}


router = DefaultRouter()

router.register(r'post', views.SimplePostDetail)
router.register(r'posts', views.SimplePostsList)
router.register(r'event', views.EventPostDetail)
router.register(r'events', views.EventPostsList)
router.register(r'user', views.UserDetail)
router.register(r'users', views.UsersList)
router.register(r'settings', views.SettingsDetail)
router.register(r'replies', views.RepliesList)
router.register(r'my-posts', views.MyPostsList)
router.register(r'my-help-posts', views.MyHelpPostList)
router.register(r'feedback', views.FeedbackList)
router.register(r'blog-post', views.BlogPostDetail)
router.register(r'blog-posts', views.BlogPostsList)
router.register(r'admin-posts-closed', views.BlockedPostsList)
router.register(r'admin-posts-moderation', views.ModeratedPostsList)

urlpatterns = [
    url(r'^auth/social/$', views.social_auth),
    url(r'^post-comments/(?P<id_post>' + REGEX_ID + ')/$', views.PostCommentList.as_view(COMMENT_PARAMS)),
    url(r'^user-comments/(?P<id_user>' + REGEX_ID + ')/$', views.UserCommentList.as_view(COMMENT_PARAMS)),
    url(r'^helpers/(?P<id_post>' + REGEX_ID + ')/$', views.HelpList.as_view(HELP_PARAMS))
]

urlpatterns_custom_djoser = [
    url(r'^auth/register/$', views.CustomRegistrationView.as_view()),
    url(r'^auth/password/reset/$', views.CustomPasswordResetView.as_view()),
]

urlpatterns += urlpatterns_custom_djoser
urlpatterns += router.urls

