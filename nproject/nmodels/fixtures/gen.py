import os
import json

orig_categories = ""
with open('orig-categories.json') as file:
    orig_categories = json.load(file)

result_categories = []
for category in orig_categories:
    for subcategory in category["subcategories"]:
        el = {
            'model': 'nmodels.Category',
            'pk': subcategory['id'],
            'fields': {
                'category_type': category['type'],
                'subcategory_type': subcategory['type']
            }
        }
        result_categories.append(el)


with open('categories.json', 'w') as file:
    file.write(json.dumps(result_categories))