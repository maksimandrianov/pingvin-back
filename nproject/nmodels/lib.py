# -*- coding: utf-8 -*-
from django.contrib.auth.models import AnonymousUser
from django.conf import settings
from django.core.mail.backends.smtp import EmailBackend as BaseEmailBackend
from nproject.settings import MY_URL
import re, importlib
from concurrent import futures
import json
import datetime


class EmailBackend(BaseEmailBackend):

    def __init__(self, *args, **kwargs):
        super(EmailBackend, self).__init__(*args, **kwargs)

        self.executor = futures.ThreadPoolExecutor(
                max_workers=int(getattr(settings, 'EMAIL_BACKEND_POOL_SIZE', 15))
        )

    def send_messages(self, email_messages):
        return self.executor.submit(super(EmailBackend, self).send_messages, email_messages)


def str_to_class(module_name, class_name):
    m = importlib.import_module(module_name)
    return getattr(m, class_name)


def plural(noun):
    if re.search('[sxz]$', noun):
        return re.sub('$', 'es', noun)
    elif re.search('[^aeioudgkprt]h$', noun):
        return re.sub('$', 'es', noun)
    elif re.search('[^aeiou]y$', noun):
        return re.sub('y$', 'ies', noun)
    else:
        return noun + 's'


def update_mtom(obj, class_name, validated_data):
    class_n = str_to_class(obj.__module__, class_name)
    field = getattr(obj, plural(class_name.lower()))
    all = field.all()
    adding_list = []
    for d in validated_data:
        is_searched = None
        for elem in all:
            if elem.is_equal(d):
                is_searched = True
                elem.is_visited = True
                break
        if is_searched is None:
            adding_list.append(class_n.objects.get_or_create(**d))
    field.remove(*[c for c in all if not hasattr(c, 'is_visited')])
    field.add(*[item[0] for item in adding_list])


def dictfetchall(cursor):
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
        ]

def url(str):
    return MY_URL + str


def is_anon(user):
    return isinstance(user, AnonymousUser)


class View:
    def __init__(self, kwargs):
        self.kwargs = kwargs


def to_json(body):
    body_unicode = body.decode('utf-8')
    return json.loads(body_unicode)

LEGEND = {
    ' ':'-',
    'а':'a',
    'б':'b',
    'в':'v',
    'г':'g',
    'д':'d',
    'е':'e',
    'ё':'yo',
    'ж':'zh',
    'з':'z',
    'и':'i',
    'й':'y',
    'к':'k',
    'л':'l',
    'м':'m',
    'н':'n',
    'о':'o',
    'п':'p',
    'р':'r',
    'с':'s',
    'т':'t',
    'у':'u',
    'ф':'f',
    'х':'h',
    'ц':'c',
    'ч':'ch',
    'ш':'sh',
    'щ':'shch',
    'ъ':'y',
    'ы':'y',
    'ь':'',
    'э':'e',
    'ю':'yu',
    'я':'ya',
    'А':'A',
    'Б':'B',
    'В':'V',
    'Г':'G',
    'Д':'D',
    'Е':'E',
    'Ё':'Yo',
    'Ж':'Zh',
    'З':'Z',
    'И':'I',
    'Й':'Y',
    'К':'K',
    'Л':'L',
    'М':'M',
    'Н':'N',
    'О':'O',
    'П':'P',
    'Р':'R',
    'С':'S',
    'Т':'T',
    'У':'U',
    'Ф':'F',
    'Х':'H',
    'Ц':'Ts',
    'Ч':'Ch',
    'Ш':'Sh',
    'Щ':'Shch',
    'Ъ':'Y',
    'Ы':'Y',
    'Ь':'',
    'Э':'E',
    'Ю':'Yu',
    'Я':'Ya',
}


def transliterate(string, dic=LEGEND):
    for i, j in dic.items():
        string = string.replace(i, j)
    return string


def make_text_link(string):
    string = string.lower()
    reg = re.compile('[^а-яА-Яa-zA-Z ]')
    string = reg.sub('', string)
    string = transliterate(string)
    return string + '-' + datetime.datetime.now().strftime("%d-%m-%y")

