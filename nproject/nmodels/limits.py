class Limits:
    # todo: add limits to mtm
    class Condition:
        max_other = 128
        min_other = 8

    class Feedback:
        max_msg = 512
        min_msg = 4

        max_browser_os = 256

    class Contact:
        max_data = 128
        min_data = 4

    class Category:
        max_cat_name = 64
        max_subcat_name = 64

    class GeoInfo:
        max_country = 64
        min_country = 2
        max_city = 64
        min_city = 2
        max_address = 128
        min_address = 8

    class User:
        max_email = 256
        max_first_name = 128
        min_first_name = 2
        max_second_name = 128
        min_second_name = 2
        max_info = 2048
        gender = 1

    class HeaderStyle:
        pass
        # max_color = 7
        # min_color = 7

    class Post:
        max_header = 256
        max_text = 2048 * 4
        min_text = 8
        date_end_expired_days = 30
        max_organization = 256

    class Comment:
        max_text = 1024
        default_rating = 5
        max_rating = 5
        min_rating = 1

    class Url:
        min_len_url = 0
        max_len_url = 512

    class Honor:
        max_name = 512

    class UserSocialInfo:
        max_id = 64