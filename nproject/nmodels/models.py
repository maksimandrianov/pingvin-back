from django.db import models, transaction
from django.db.models import Q, F
from django.utils import timezone
from django.contrib.gis.db import models as gis_models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from datetime import datetime, timedelta
from django.contrib.gis.geos import Point
from geopy.distance import distance
from sphinxql.query import QuerySet, SearchQuerySet
from nmodels.limits import Limits
from nmodels import lib
from nproject.settings import USE_INC_POST, DEBUG
import random, time

# TODO admin interface
# TODO write description protocol
# TODO filldb !!!
# TODO ADD MULTIMEDIA
# TODO MAX COUNT CATEGORIES, CONTACTS

# from caching.base import CachingManager, CachingMixin
# http://ruhighload.com/
# authorization as https://github.com/sunscrapers/djoser
# Register a new user: curl -X POST http://127.0.0.1:8088/auth/register/ --data 'username=djoser&password=djoser'
# Let's log in: curl -X POST http://127.0.0.1:8088/auth/login/ --data 'username=djoser&password=djoser'
# curl -X GET http://127.0.0.1:8088/auth/me/ -H 'Authorization: Token b704c9fc3655635646356ac2950269f352ea1139'
# https://www.digitalocean.com/community/tutorials/how-to-install-solr-5-2-1-on-ubuntu-14-04

# TODO optimization https://www.dabapps.com/blog/api-performance-profiling-django-rest-framework/


class StatusType:
    NEW_HELPER = 0
    UPDATE_CONDITION = 1
    SELECT_HELPER = 2
    CANCEL_HELPER = 3
    FINIS_HELP = 4


class ConditionType:
    THANKS = 1
    MONEY = 2
    OTHER = 3


class PostType:
    SIMPLE = 0
    EVENT = 1


class MediaType:
    AVATAR = 1
    POST_IMG = 2
    IMG = 3
    VIDEO = 4
    SOCIAL_AVATAR = 5


class FeedbackType:
    ERROR = 0
    ADVICE = 1
    QUESTION = 2
    SIMPLE = 3


class Timestampable(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        abstract = True


class Likable(models.Model):
    like = models.PositiveIntegerField(default=0)
    dislike = models.PositiveIntegerField(default=0)

    def liked(self):
        self.like = F('like') + 1
        self.save(update_fields=['like'])

    def disliked(self):
        self.dislike = F('dislike') + 1
        self.save(update_fields=['dislike'])

    def cancel_liked(self):
        self.like = F('like') - 1
        self.save(update_fields=['like'])

    def cancel_disliked(self):
        self.dislike = F('dislike') - 1
        self.save(update_fields=['dislike'])

    class Meta:
        abstract = True


class Like(models.Model):
    user = models.ForeignKey('User', related_name='+')
    is_like = models.NullBooleanField(default=None)

    @staticmethod
    def islike(obj, user_id):
        cls = obj.like_class
        try:
            lk = cls.objects.get(user_id=user_id, obj=obj).values('is_like')
            return lk.get('is_like')
        except cls.DoesNotExist:
            return None

    @staticmethod
    def like(object_class, object_id, user_id):
        obj = object_class.objects.get(id=object_id)
        cls = obj.like_class
        lk, created = cls.objects.get_or_create(user_id=user_id, obj=obj)
        if lk.is_like is None:
            obj.liked()
            lk.is_like = True
        elif lk.is_like:
            obj.cancel_liked()
            lk.is_like = None
        elif not lk.is_like:
            obj.cancel_disliked()
            obj.liked()
            lk.is_like = True
        lk.save(update_fields=['is_like'])
        return lk

    @staticmethod
    def dislike(object_class, object_id, user_id):
        obj = object_class.objects.get(id=object_id)
        cls = obj.like_class
        lk, created = cls.objects.get_or_create(user_id=user_id, obj=obj)
        if lk.is_like is None:
            obj.disliked()
            lk.is_like = False
        elif lk.is_like:
            obj.cancel_liked()
            obj.disliked()
            lk.is_like = False
        elif not lk.is_like:
            obj.cancel_disliked()
            lk.is_like = None
        lk.save()
        return lk

    class Meta:
        abstract = True


class LikeBlogPost(Like):
    obj = models.ForeignKey('BlogPost', related_name='+')

    class Meta:
        unique_together = ('user', 'obj')


class LikePost(Like):
    obj = models.ForeignKey('Post', related_name='+')

    class Meta:
        unique_together = ('user', 'obj')


class LikeCommentUser(Like):
    obj = models.ForeignKey('CommentUser', related_name='+')

    class Meta:
        unique_together = ('user', 'obj')


class LikeCommentPost(Like):
    obj = models.ForeignKey('CommentPost', related_name='+')

    class Meta:
        unique_together = ('user', 'obj')


class MediaUrl(models.Model):
    type = models.SmallIntegerField(default=MediaType.IMG)
    url = models.CharField(max_length=Limits.Url.max_len_url, blank=True)


class Condition(models.Model):
    type = models.SmallIntegerField(default=ConditionType.THANKS)
    money = models.PositiveIntegerField(blank=True, default=0)
    other = models.CharField(max_length=Limits.Condition.max_other, blank=True)

    class Meta:
        unique_together = ('type', 'money', 'other')

    @staticmethod
    def create_thanks():
        return Condition.objects.get_or_create()

    @staticmethod
    def create_money(money):
        return Condition.objects.get_or_create(type=ConditionType.MONEY, money=money)

    @staticmethod
    def create_alternative(other):
        return Condition.objects.get_or_create(type=ConditionType.OTHER, other=other)


class Contact(models.Model):
    type = models.PositiveSmallIntegerField(default=0)
    data = models.CharField(max_length=Limits.Contact.max_data)

    def is_equal(self, other):
        if isinstance(other, self.__class__):
            return self.type == other.type and self.type == other.type
        else:
            return self.type == other.get('type') and self.type == other.get('type')


class Feedback(Timestampable):
    type = models.PositiveSmallIntegerField(default=0)
    message = models.CharField(max_length=Limits.Feedback.max_msg, blank=True)
    url = models.CharField(max_length=Limits.Url.max_len_url, blank=True)
    created_user = models.ForeignKey('User', related_name='+')
    rating = models.PositiveSmallIntegerField(default=0)
    platform = models.CharField(max_length=Limits.Feedback.max_browser_os, blank=True)
    email = models.EmailField(max_length=Limits.User.max_email, blank=True)


def timezone_now_delta_1h():
    now = timezone.now()
    return now if DEBUG else now + timezone.timedelta(hours=1)


def timezone_now_delta_1d():
    now = timezone.now()
    return now if DEBUG else now + timezone.timedelta(days=1)


class Prompt(models.Model):
    MINIMAL_TIME_PROMPT_HOUR = 24
    MAXIMUM_TIME_PROMPT_HOUR = 7 * 24

    user = models.OneToOneField('User')
    next_prompt_feedback = models.DateTimeField(null=True, default=timezone_now_delta_1d)
    next_prompt_help = models.DateTimeField(null=True, default=timezone_now_delta_1h)
    next_prompt_create_publication = models.DateTimeField(null=True, default=timezone_now_delta_1h)

    @staticmethod
    def get_rand_hours():
        return random.randint(Prompt.MINIMAL_TIME_PROMPT_HOUR,
                              Prompt.MAXIMUM_TIME_PROMPT_HOUR)

    @staticmethod
    def get_prompt(user):
        prompt, created = Prompt.objects.get_or_create(user=user)
        return prompt

    def prompt_feedback(self):
        result = {}
        now = datetime.now()

        if self.next_prompt_feedback is None or now < self.next_prompt_feedback:
            result['has_feedback'] = True
        else:
            has_feedback = self.user.has_feedback()
            if has_feedback:
                self.next_prompt_feedback = None
                self.save(update_fields=['next_prompt_feedback'])
            result['has_feedback'] = has_feedback
        return result

    def defer_prompt_feedback(self):
        delta = timezone.timedelta(hours=Prompt.get_rand_hours())
        self.next_prompt_feedback = timezone.now() + delta
        self.save(update_fields=['next_prompt_feedback'])
        return {'has_feedback': True}

    def skip_prompt_feedback(self):
        self.next_prompt_feedback = None
        self.save(update_fields=['next_prompt_feedback'])
        return {'has_feedback': True}

    def prompt_publications(self):
        result = {}
        now = timezone.now()
        if self.next_prompt_create_publication is None or \
                        now < self.next_prompt_create_publication:
            result['has_publication'] = True
        else:
            has_publication = self.user.has_publications()
            if has_publication:
                self.next_prompt_create_publication = None
                self.save(update_fields=['next_prompt_create_publication'])
            result['has_publication'] = has_publication
        return result

    def defer_prompt_publications(self):
        delta = timezone.timedelta(hours=Prompt.get_rand_hours())
        self.next_prompt_create_publication = timezone.now() + delta
        self.save(update_fields=['next_prompt_create_publication'])
        return {'has_publication': True}

    def skip_prompt_publications(self):
        self.next_prompt_feedback = None
        self.save(update_fields=['next_prompt_create_publication'])
        return {'has_publication': True}

    def prompt_helps(self):
        result = {}
        now = datetime.now()
        if self.next_prompt_help is None or now < self.next_prompt_help:
            result['has_help'] = True
        else:
            has_help = self.user.has_helps()
            if has_help:
                self.next_prompt_help = None
                self.save(update_fields=['next_prompt_help'])
            result['has_help'] = has_help
        return result

    def defer_prompt_helps(self):
        delta = timezone.timedelta(hours=Prompt.get_rand_hours())
        self.next_prompt_help = timezone.now() + delta
        self.save(update_fields=['next_prompt_help'])
        return {'has_help': True}

    def skip_prompt_helps(self):
        self.next_prompt_help = None
        self.save(update_fields=['next_prompt_help'])
        return {'has_help': True}


class Category(models.Model):
    category_type = models.PositiveSmallIntegerField(default=0)
    subcategory_type = models.PositiveSmallIntegerField(default=0)
    other_category_name = models.CharField(max_length=Limits.Category.max_cat_name, blank=True)
    other_subcategory_name = models.CharField(max_length=Limits.Category.max_subcat_name, blank=True)

    class Meta:
        unique_together = ('category_type', 'subcategory_type')

    def is_equal(self, other):
        if isinstance(other, self.__class__):
            return self.category_type == other.category_type and \
                   self.subcategory_type == other.subcategory_type
        else:
            return self.category_type == other.get('category_type') and \
                   self.subcategory_type == other.get('subcategory_type')


# todo: realize
class Honor(models.Model):
    type = models.PositiveSmallIntegerField(default=0)
    level = models.PositiveSmallIntegerField(default=0)
    name = models.CharField(max_length=Limits.Honor.max_name, blank=True)

    class Meta:
        unique_together = ('type', 'name', 'level')


class Help(Timestampable):
    post = models.ForeignKey('Post', related_name='+')
    user = models.ForeignKey('User', related_name='+')
    is_head = models.BooleanField(default=False)
    condition = models.ForeignKey(Condition, related_name='+', null=True)
    status = models.SmallIntegerField(default=StatusType.NEW_HELPER)

    @staticmethod
    def check_status(owner, status, new_status):
        if owner:
            if (status == StatusType.NEW_HELPER or status == StatusType.UPDATE_CONDITION) \
                    and new_status == StatusType.SELECT_HELPER:
                return True
            if status == StatusType.SELECT_HELPER and \
                    (new_status == StatusType.CANCEL_HELPER or new_status == StatusType.FINIS_HELP):
                return True
            if status == StatusType.CANCEL_HELPER and new_status == StatusType.SELECT_HELPER:
                return True
        else:
            if (status == StatusType.NEW_HELPER or
                        status == StatusType.CANCEL_HELPER or
                        status == StatusType.UPDATE_CONDITION) \
                    and new_status == StatusType.UPDATE_CONDITION:
                return True
        return False

    def __str__(self):
        return 'post {}, user {}, condition {}, status {}' \
            .format(self.post.id, self.user.id,self.condition.id, self.status)

        # class Meta:
        #     unique_together = ('post', 'user')


class GeoInfo(gis_models.Model):
    country = models.CharField(max_length=Limits.GeoInfo.max_country, blank=True)
    city = models.CharField(max_length=Limits.GeoInfo.max_city, blank=True)
    point = gis_models.PointField(geography=True, srid=4326, default='POINT(0.0 0.0)')
    address = models.CharField(max_length=Limits.GeoInfo.max_address, blank=True)

    objects = gis_models.GeoManager()


class UserSettings(Timestampable):
    id = models.OneToOneField('User', primary_key=True, related_name='settings')
    categories = models.ManyToManyField(Category)  # showed categories
    contacts = models.ManyToManyField(Contact)  # showed contacts
    location = models.OneToOneField(GeoInfo)
    only_custom_header = models.BooleanField(default=False)

    def update_categories(self, categories_id):
        n_categories = Category.objects.filter(pk__in=categories_id)
        categories = self.categories.all()
        self.categories.remove(*(set(categories) - set(n_categories)))
        self.categories.add(*(set(n_categories) - set(categories)))

    def update_contacts(self, contacts_id):
        n_contacts = Contact.objects.filter(pk__in=contacts_id)
        contacts = self.contacts.all()
        self.contacts.remove(*(set(contacts) - set(n_contacts)))
        self.contacts.add(*(set(n_contacts) - set(contacts)))

    def sync_categories(self, user):
        self.categories.remove(*(set(self.categories.all()) - set(user.categories.all())))

    def sync_contacts(self, user):
        self.contacts.remove(*(set(self.contacts.all()) - set(user.contacts.all())))

    def is_set_location(self):
        return self.location.point != Point(0.0, 0.0)

    def apply_settings_category(self, posts_queryset):
        if self.categories.all().exists():
            category_ids = self.categories.all().values_list('pk', flat=True)
            posts_queryset = posts_queryset.filter(category__in=category_ids)
        return posts_queryset

    def save(self, *args, **kwargs):
        try:
            self.location
        except GeoInfo.DoesNotExist:
            self.location = GeoInfo.objects.create()
        super(self.__class__, self).save(*args, **kwargs)


class UserManager(BaseUserManager):

    def create_user(self, email, password, first_name, second_name):
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(email=self.normalize_email(email),first_name=first_name, second_name=second_name)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, first_name, second_name):
        user = self.create_user(email, password, first_name, second_name)
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class UserSocialInfo(models.Model):

    SOCIAL_NET = (
        (1, 'vk'),
        (2, 'fb'),
        (3, 'tw'),
        (4, 'gp'),
    )

    sn_type = models.SmallIntegerField(choices=SOCIAL_NET)
    user_id = models.CharField(max_length=Limits.UserSocialInfo.max_id)
    email = models.EmailField(max_length=Limits.User.max_email, null=True, blank=True)

    class Meta:
        unique_together = ('sn_type', 'user_id')


class User(AbstractBaseUser, PermissionsMixin, Timestampable, Likable):

    GENDER = (
        ('x', 'ni'),
        ('m', 'male'),
        ('f', 'female'),
    )

    # required for the first step, fill in the registration
    email = models.EmailField(max_length=Limits.User.max_email, unique=True)
    first_name = models.CharField(max_length=Limits.User.max_first_name)
    second_name = models.CharField(max_length=Limits.User.max_second_name)
    social = models.OneToOneField(UserSocialInfo, null=True, blank=True)

    avatar = models.ForeignKey(MediaUrl, null=True, related_name='+')
    avatar_orig = models.ForeignKey(MediaUrl, null=True, related_name='+')

    bday = models.DateField(null=True)
    gender = models.CharField(max_length=Limits.User.gender, default='x', choices=GENDER)
    info = models.CharField(max_length=Limits.User.max_info, blank=True)
    contacts = models.ManyToManyField(Contact)
    categories = models.ManyToManyField(Category)
    media = models.ManyToManyField(MediaUrl)
    # involvement
    mhonors = models.ManyToManyField(Honor)
    rank = models.PositiveSmallIntegerField(default=0)
    # statistics
    honors = models.PositiveIntegerField(default=0)
    helps = models.PositiveIntegerField(default=0)
    mhelps = models.PositiveIntegerField(default=0)
    views = models.PositiveIntegerField(default=0)
    posts = models.PositiveIntegerField(default=0)

    activity = models.DateTimeField(default=timezone.now)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'second_name']

    objects = UserManager()

    def save(self, *args, **kwargs):
        super(self.__class__, self).save(*args, **kwargs)
        try:
            self.settings
        except UserSettings.DoesNotExist:
            UserSettings.objects.create(id=self)

    def get_username(self):
        return self.first_name

    @staticmethod
    def activated():
        return User.objects.filter(is_active=True)

    @transaction.atomic
    def inc_helped_me(self):
        self.mhelps ++ 1
        self.save(update_fields=['mhelps'])

    @transaction.atomic
    def inc_helps(self):
        self.helps += 1
        self.save(update_fields=['helps'])

    @transaction.atomic
    def inc_posts(self):
        self.posts += 1
        self.save(update_fields=['posts'])

    @transaction.atomic
    def inc_views(self):
        if not USE_INC_POST:
            return
        self.views += 1
        self.save(update_fields=['views'])

    def update_contacts_sync(self, contacts_dict):
        self._update_contacts(contacts_dict)
        try:
            self.settings.sync_contacts(self)
        except UserSettings.DoesNotExist:
            pass

    def update_categories_sync(self, categories_dict):
        self._update_categories(categories_dict)
        try:
            self.settings.sync_categories(self)
        except UserSettings.DoesNotExist:
            pass

    # for user who wants to help
    def help(self, post_id, condition_dict):
        help = None
        condition, created = Condition.objects.get_or_create(**condition_dict)
        with transaction.atomic():
            if Help.objects.filter(user_id=self.pk, post_id=post_id,
                                   condition=condition, is_head=True).exists():
                raise ValueError('help object already created')
            help = Help.objects.create(user_id=self.pk, post_id=post_id,
                                       condition=condition, is_head=True)
        help.id = None
        help.is_head = False
        help.save()
        help.post.inc_helpers()
        return help

    def cancel_help(self, post_id):
        help = Help.objects.get(user_id=self.pk, post_id=post_id, is_head=True)
        help.post.dec_helpers()
        help.delete()

    def update_condition_help(self, post_id, condition_dict):
        updated_head = Help.objects.get(user_id=self.pk, post_id=post_id, is_head=True)
        if not Help.check_status(False, updated_head.status, StatusType.UPDATE_CONDITION):
            raise Help.DoesNotExist
        condition, created = Condition.objects.get_or_create(**condition_dict)
        help = Help.objects.create(user_id=self.pk, post_id=post_id,
                                   condition=condition, status=StatusType.UPDATE_CONDITION)
        updated_head.status = help.status
        updated_head.condition = help.condition
        updated_head.created = help.created
        updated_head.updated = help.updated
        updated_head.save()
        return help

    def like_post(self, post_id):
        return self._like(Post, post_id)

    def dislike_post(self, post_id):
        return self._dislike(Post, post_id)

    def like_comment_post(self, comment_id):
        return self._like(CommentPost, comment_id)

    def dislike_comment_post(self, comment_id):
        return self._dislike(CommentPost, comment_id)

    def like_comment_user(self, comment_id):
        return self._like(CommentUser, comment_id)

    def dislike_comment_user(self, comment_id):
        return self._dislike(CommentUser, comment_id)

    def like_blog_post(self, blog_post_id):
        return self._like(BlogPost, blog_post_id)

    def dislike_blog_post(self, blog_post_id):
        return self._dislike(BlogPost, blog_post_id)

    def moderation_over(self, post_id):
        post = Post.objects.get(id=post_id)
        post.moderation_over()
        return post

    # for owner post
    def close(self, post_id):
        post = Post.objects.get(id=post_id)
        post.is_actually = False
        updated_fields = ['is_actually']
        if self.is_admin:
            post.is_blocked = True
            updated_fields += ['is_blocked']
        post.save(update_fields=updated_fields)
        return post

    def unblock(self, post_id):
        post = Post.objects.get(id=post_id)
        post.is_blocked = False
        post.is_actually = True
        post.save(update_fields=['is_actually', 'is_blocked'])
        return post

    def select_helper(self, post_id, user_id):
        return self._choice_help(post_id, user_id, StatusType.SELECT_HELPER)

    def cancel_helper(self, post_id, user_id):
        return self._choice_help(post_id, user_id, StatusType.CANCEL_HELPER)

    def select_successfully(self, post_id, user_id):
        updated_head = Help.objects.get(user_id=user_id, post_id=post_id,
                                        is_head=True)
        if not Help.check_status(True, updated_head.status, StatusType.FINIS_HELP):
            raise Help.DoesNotExist
        help = Help.objects.create(post_id=post_id, user_id=user_id, status=StatusType.FINIS_HELP)
        help.user.inc_helps()
        help.post.created_user.inc_helped_me()
        updated_head.status = help.status
        updated_head.created = help.created
        updated_head.updated = help.updated
        updated_head.save()
        return help

    def replies(self):
        return self._all_replies_q()

    def count_new_replies(self):
        all_replies = self._all_replies_q()
        all_replies = all_replies.filter(updated__gt=self.activity)
        return all_replies.count()

    def see_replies(self):
        self.activity = timezone.now()
        self.save()

    def public_contacts(self):
        if self.settings.contacts.all().exists():
            return self.settings.contacts
        return self.contacts

    def my_posts(self):
        return Post.objects.filter(created_user=self)

    def my_help_posts(self):
        posts = self._my_helps().values_list('post__pk', flat=True)
        return Post.objects.filter(id__in=posts)

    @staticmethod
    def search(params):
        from nmodels.indexes import UserIndex
        queryset = SearchQuerySet(UserIndex)

        if params.get('city') is not None:
            queryset = queryset.search('@location ' + params['city'])
        if params.get('country') is not None:
            queryset = queryset.search('@location ' + params['country'])
        if params.get('name') is not None:
            queryset = queryset.search('@full_name ' + params['name'])

        return queryset

    def _choice_help(self, post_id, user_id, status):
        updated_head = Help.objects.get(user_id=user_id, post_id=post_id,
                                        is_head=True)
        if not Help.check_status(True, updated_head.status, status):
            raise Help.DoesNotExist
        help = Help.objects.create(post_id=post_id, user_id=user_id, status=status)
        updated_head.status = help.status
        updated_head.created = help.created
        updated_head.updated = help.updated
        updated_head.save()
        return help

    def _my_helps(self):
        return Help.objects.filter(user=self, is_head=False)

    def _replies_to_posts_q(self):
        posts = self.my_posts().values_list('pk', flat=True)
        helps = Help.objects.filter(post__in=posts)
        helps = helps.filter(is_head=False)
        helps = helps.filter(
                Q(status=StatusType.NEW_HELPER) |
                Q(status=StatusType.UPDATE_CONDITION)
        )

        return helps

    def _replies_to_help_q(self):
        helps = self._my_helps().exclude(post__created_user=self)
        helps = helps.filter(
                Q(status=StatusType.SELECT_HELPER) |
                Q(status=StatusType.CANCEL_HELPER) |
                Q(status=StatusType.FINIS_HELP)
        )

        return helps

    def _all_replies_q(self):
        return self._replies_to_posts_q() | self._replies_to_help_q()

    def _update_contacts(self, contacts_dict):
        lib.update_mtom(self, 'Contact', contacts_dict)

    def _update_categories(self, categories_dict):
        lib.update_mtom(self, 'Category', categories_dict)

    def _like(self, class_name, object_id):
        return Like.like(class_name, object_id, self.pk)

    def _dislike(self, class_name, object_id):
        return Like.dislike(class_name, object_id, self.pk)

    def get_full_name(self):
        return self.first_name + ' ' + self.second_name

    def get_short_name(self):
        return self.first_name

    def old_year(self):
        if self.bday is None:
            raise ValueError('bday is None')
        return datetime.now().year - self.bday.year
    old_year.short_description = 'Old years'

    @property
    def is_staff(self):
        return self.is_admin

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def has_feedback(self):
        return Feedback.objects.filter(created_user=self, type=FeedbackType.SIMPLE).exists()

    def has_publications(self):
        return Post.objects.filter(created_user=self).exists()

    def has_helps(self):
        return Help.objects.filter(user=self).exists()


class HeaderStyle(Timestampable):
    img = models.CharField(max_length=Limits.Url.max_len_url, blank=True)
    img_orig = models.CharField(max_length=Limits.Url.max_len_url, blank=True)
    b_color = models.PositiveIntegerField(default=0)


class BlogPost(Timestampable, Likable):
    created_user = models.ForeignKey(User, related_name='+')
    text = models.CharField(max_length=Limits.Post.max_text)
    stext = models.CharField(max_length=Limits.Post.max_text, default='')
    header = models.CharField(max_length=Limits.Post.max_header, blank=True)
    link = models.CharField(max_length=Limits.Post.max_header, blank=True, db_index=True, unique=True)
    h_style = models.ForeignKey(HeaderStyle, null=True, related_name='+')

    like_class = LikeBlogPost

    def is_like(self, user_id):
        return Like.islike(self, user_id)


def timezone_now_delta_expired_days():
    dt = timezone.now() + timezone.timedelta(
            days=Limits.Post.date_end_expired_days)
    return dt.date()


class Post(Timestampable, Likable):
    type = models.SmallIntegerField(default=PostType.SIMPLE)

    # Common field for all post types
    # required for the first step
    created_user = models.ForeignKey(User, related_name='+')
    text = models.CharField(max_length=Limits.Post.max_text)

    header = models.CharField(max_length=Limits.Post.max_header, blank=True)
    h_style = models.ForeignKey(HeaderStyle, null=True, related_name='+')
    location = models.OneToOneField(GeoInfo, null=True)
    category = models.ForeignKey(Category, null=True, related_name='+')
    condition = models.ForeignKey(Condition, null=True, related_name='+')
    date_end = models.DateField(default=timezone_now_delta_expired_days)
    media = models.ManyToManyField(MediaUrl)
    req_helpers = models.PositiveSmallIntegerField(default=1)

    helpers = models.PositiveIntegerField(default=0)  # filed for optimization
    views = models.PositiveIntegerField(default=0)
    is_actually = models.BooleanField(default=True)
    is_blocked = models.BooleanField(default=False)

    # Fields for EVENT type
    is_moderated = models.BooleanField(default=False)
    moderated_time = models.DateTimeField(null=True)
    organization = models.CharField(max_length=Limits.Post.max_organization, blank=True)

    like_class = LikePost

    objects = gis_models.GeoManager()

    def save(self, *args, **kwargs):
        if self.type == PostType.EVENT and self.is_moderated \
                and self.moderated_time is None:
            self.moderated_time = timezone.now()
        super(self.__class__, self).save(*args, **kwargs)

    @classmethod
    def from_db(cls, db, field_names, values):
        obj = super(Post, cls).from_db(db, field_names, values)
        if obj.is_actually and obj.date_end < timezone.now().date():
            obj.is_actually = False
        return obj

    @transaction.atomic
    def inc_views(self):
        self.views += 1
        self.save(update_fields=['views'])

    @transaction.atomic
    def inc_helpers(self):
        self.helpers += 1
        self.save(update_fields=['helpers'])

    @transaction.atomic
    def dec_helpers(self):
        self.helpers -= 1
        self.save(update_fields=['helpers'])

    def all_helpers(self):
        return Help.objects.filter(post=self, is_head=True)

    def calc_distance(self, user_settings):
        if self.location is None or user_settings.location.point == Point(0.0, 0.0):
            return None
        return distance(self.location.point, user_settings.location.point).meters

    def is_like(self, user_id):
        return Like.islike(self, user_id)

    def get_helpers(self):
        return Help.objects.filter(post=self, is_head=True) \
                   .order_by('?') \
                   .select_related('user') \
                   .only('user__first_name', 'user__second_name','user__avatar', 'post_id')[:3]

    @staticmethod
    def get_group_liked(user_id, post_ids):
        return LikePost.objects \
            .filter(user_id=user_id, obj__in=post_ids) \
            .only('post_id', 'is_like') \
            .values()

    def is_helped(self, user_id):
        return Help.objects.filter(user_id=user_id, post_id=self.pk, is_head=True).exists()

    @staticmethod
    def get_group_helped(user_id, post_ids):
        return Help.objects \
            .filter(user_id=user_id, post__in=post_ids, is_head=True) \
            .values_list('post__id', flat=True)

    def moderation_over(self):
        self.is_moderated = True
        self.save(update_fields=['is_moderated'])

    @staticmethod
    def actually_q(is_actually=True):
        if is_actually:
            return Post.objects.filter(is_actually=is_actually)
        return Post.objects.all()

    @staticmethod
    def all_simple_posts():
        return Post.objects.filter(type=PostType.SIMPLE)

    @staticmethod
    def all_events():
        return Post.objects.filter(type=PostType.EVENT)

    @staticmethod
    def simple_posts(is_actually=True):
        return Post.actually_q(is_actually).filter(type=PostType.SIMPLE)

    @staticmethod
    def event_posts():
        return Post.actually_q().filter(type=PostType.EVENT) \
            .filter(is_moderated=True)

    @staticmethod
    def non_moderated_posts():
        return Post.actually_q().filter(is_moderated=False)

    @staticmethod
    def blocked_posts():
        return Post.objects.filter(is_blocked=True)

    @staticmethod
    def event(pk):
        posts = Post.objects.filter(pk=pk)
        if len(posts) != 1:
            Post.DoesNotExist()
        post = posts[0]
        if post.type == PostType.EVENT:
            return posts
        else:
            raise Post.DoesNotExist()

    @staticmethod
    def search(params):
        from nmodels.indexes import PostIndex
        queryset = SearchQuerySet(PostIndex)  # actually posts

        if params.get('type') is not None:
            queryset = queryset.filter(type=params['type'])
        if params.get('motivation') is not None:
            queryset = Post._search_condition(queryset, params)
        if params.get('category') is not None:
            queryset = Post._search_category(queryset, params)
        if params.get('search') is not None:
            queryset = queryset.search('@full ' + params['search'])

        return queryset

    @staticmethod
    def _search_condition(queryset, params):
        if params.get('motivation') is not None:
            queryset = queryset.filter(condition__type=params['motivation'])
            if params['motivation'] == ConditionType.MONEY and \
                            params.get('money') is not None:
                queryset = queryset.filter(condition__money=params['money'])
            elif params['motivation'] == ConditionType.OTHER and \
                            params.get('other') is not None:
                queryset = queryset.search('@other_condition ' + params['other'])

        return queryset

    @staticmethod
    def _search_category(queryset, params):
        if params.get('category') is not None:
            queryset = queryset.filter(category__category_type=params['category'])
            if params.get('subcategory') is not None:
                queryset = queryset.filter(
                        category__subcategory_type=params['subcategory'])

        return queryset


class Comment(Timestampable, Likable):
    created_user = models.ForeignKey(User, related_name='+')
    answered_him = models.ForeignKey(User, related_name='+', null=True)

    class Meta:
        abstract = True


class CommentUser(Comment):
    text = models.CharField(blank=True, max_length=Limits.Comment.max_text)
    user = models.ForeignKey(User, related_name='+')
    rating = models.SmallIntegerField(default=Limits.Comment.default_rating)

    like_class = LikeCommentUser

    def is_like(self, user_id):
        return Like.islike(self, user_id)

    class Meta:
        ordering = ('-created', )


class CommentPost(Comment):
    text = models.CharField(max_length=Limits.Comment.max_text)  # not blank
    post = models.ForeignKey(Post, related_name='+')

    like_class = LikeCommentPost

    def is_like(self, user_id):
        return Like.islike(self, user_id)

    class Meta:
        ordering = ('-created', )
