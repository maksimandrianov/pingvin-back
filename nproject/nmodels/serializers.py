from rest_framework import serializers
from drf_extra_fields.geo_fields import PointField
from nmodels.lib import is_anon, make_text_link
from nmodels.models import User, Post, Category, MediaUrl, CommentUser, \
    CommentPost, StatusType, PostType, MediaType
from nmodels.models import Condition, Help, Contact, UserSettings, GeoInfo, \
    HeaderStyle, Feedback, Honor, BlogPost, FeedbackType
from nmodels.permissions import UserCommentPermission
from nmodels.limits import Limits
from nmodels.querysets import Qw
import html2text


class HonorSerializer(serializers.ModelSerializer):
    type = serializers.IntegerField()
    name = serializers.CharField(max_length=Limits.Honor.max_name)

    class Meta:
        model = Honor
        fields = ('type', 'name')


class ContactsSerializer(serializers.ModelSerializer):
    type = serializers.IntegerField()
    data = serializers.CharField(max_length=Limits.Contact.max_data,
                                 min_length=Limits.Contact.min_data)

    class Meta:
        model = Contact
        fields = ('id', 'type', 'data')


class CategorySerializer(serializers.ModelSerializer):
    category_type = serializers.IntegerField()
    subcategory_type = serializers.IntegerField()

    def get_unique_together_validators(self):
        return []

    class Meta:
        model = Category
        fields = ('id', 'category_type', 'subcategory_type')


class MediaUrlSerializer(serializers.ModelSerializer):
    type = serializers.IntegerField(required=False)
    url = serializers.CharField(required=False, allow_blank=True,
                                min_length=Limits.Url.min_len_url,
                                max_length=Limits.Url.max_len_url)

    class Meta:
        model = MediaUrl
        fields = ('type', 'url')


class HeaderStyleSerializer(serializers.ModelSerializer):
    img = serializers.CharField(required=False, min_length=Limits.Url.min_len_url,
                                max_length=Limits.Url.max_len_url)
    b_color = serializers.IntegerField(required=False)

    class Meta:
        model = HeaderStyle
        fields = ('id', 'img', 'b_color')


class ConditionSerializer(serializers.ModelSerializer):
    type = serializers.IntegerField()
    money = serializers.IntegerField(required=False, default=0)
    other = serializers.CharField(required=False, allow_blank=True,
                                  max_length=Limits.Condition.max_other,
                                  min_length=Limits.Condition.min_other)

    def get_unique_together_validators(self):
        return []

    class Meta:
        model = Condition
        fields = ('id', 'type', 'money', 'other')


class GeoInfoSerializer(serializers.ModelSerializer):
    country = serializers.CharField(required=False, allow_blank=True,
                                    max_length=Limits.GeoInfo.max_country,
                                    min_length=Limits.GeoInfo.min_country)
    city = serializers.CharField(required=False, allow_blank=True,
                                 max_length=Limits.GeoInfo.max_city,
                                 min_length=Limits.GeoInfo.min_city)
    address = serializers.CharField(required=False, allow_blank=True,
                                    max_length=Limits.GeoInfo.max_address,
                                    min_length=Limits.GeoInfo.min_address)
    point = PointField(required=False)

    class Meta:
        model = GeoInfo
        fields = ('country', 'city', 'address', 'point')


class UserSettingsSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True, required=False)
    contacts = ContactsSerializer(many=True, required=False)
    location = GeoInfoSerializer(many=False, required=False)
    # service field to indicate private data
    categories_ids = serializers.ListField(child=serializers.IntegerField(), required=False)
    contacts_ids = serializers.ListField(child=serializers.IntegerField(), required=False)

    class Meta:
        model = UserSettings
        fields = ('categories', 'contacts', 'location', 'categories_ids',
                  'contacts_ids', 'only_custom_header')

    def update(self, instance, validated_data):
        if validated_data.get('categories_ids') is not None:
            instance.update_categories(validated_data['categories_ids'])
            del validated_data['categories_ids']

        if validated_data.get('contacts_ids') is not None:
            instance.update_contacts(validated_data['contacts_ids'])
            del validated_data['contacts_ids']

        if validated_data.get('location') is not None:
            instance.location.__dict__.update(**validated_data['location'])
            instance.location.save()
            del validated_data['location']

        return super(self.__class__, self).update(instance, validated_data)


class UserMinSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=False,
                                       max_length=Limits.User.max_first_name,
                                       min_length=Limits.User.min_first_name)
    second_name = serializers.CharField(required=False,
                                        max_length=Limits.User.max_second_name,
                                        min_length=Limits.User.min_second_name)
    avatar = MediaUrlSerializer(many=False, required=False)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'second_name', 'avatar')


class MeSerializer(UserMinSerializer):
    is_admin = serializers.BooleanField(read_only=True)

    class Meta(UserMinSerializer.Meta):
        fields = UserMinSerializer.Meta.fields + ('is_admin',)


class UsersSerializer(UserMinSerializer):
    # categories = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = UserMinSerializer.Meta.fields + ('helps', 'mhelps', 'honors')


class UserDetailSerializer(UserMinSerializer):
    gender = serializers.CharField(allow_blank=True, required=False, max_length=Limits.User.gender,
                                   min_length=Limits.User.gender)
    info = serializers.CharField(required=False, allow_blank=True, max_length=Limits.User.max_info)
    bday = serializers.DateField(format='%Y-%m-%d', required=False, allow_null=True)
    contacts = ContactsSerializer(many=True, required=False)
    categories = CategorySerializer(many=True, required=False)
    mhonors = HonorSerializer(many=True, required=False, read_only=True)
    media = MediaUrlSerializer(many=True, read_only=True, required=False)

    class Meta(UserMinSerializer.Meta):
        fields = ('id', 'first_name', 'second_name', 'bday', 'gender',
                  'info','rank', 'categories', 'mhonors', 'contacts', 'media',
                  'created', 'updated', 'like', 'dislike', 'avatar')

    def update_img(self, instance, validated_data, field_name):
        if validated_data.get(field_name) is not None:
            field = getattr(instance, field_name)
            if field is None:
                setattr(instance, field_name, MediaUrl.objects.create(type=MediaType.AVATAR,
                                                                      **validated_data[field_name]))
            else:
                field.url = validated_data[field_name]['url']
                field.type = MediaType.AVATAR
                field.save()
            del validated_data[field_name]

    def update(self, instance, validated_data):
        if validated_data.get('categories') is not None:
            instance.update_categories_sync(validated_data['categories'])
            del validated_data['categories']
        if validated_data.get('contacts') is not None:
            instance.update_contacts_sync(validated_data['contacts'])
            del validated_data['contacts']
        self.update_img(instance, validated_data, 'avatar')
        self.update_img(instance, validated_data, 'avatar_orig')
        return super(self.__class__, self).update(instance, validated_data)


class BlogPostSerializer(serializers.ModelSerializer):
    created_user = UserMinSerializer(many=False, required=False)
    # is_like = serializers.SerializerMethodField('calc_is_liked')
    header = serializers.CharField(required=False, allow_blank=True, max_length=Limits.Post.max_header)
    stext = serializers.CharField(max_length=Limits.Post.max_text, min_length=Limits.Post.min_text, required=False)
    link = serializers.CharField(max_length=Limits.Post.max_header, read_only=True, required=False)
    h_style = HeaderStyleSerializer(many=False, read_only=True, required=False)

    class Meta:
        model = BlogPost
        fields = ('id', 'header', 'stext',  'created_user', 'created', 'link', 'h_style')

    # def calc_is_liked(self, obj):
    #     if is_anon(self.context['request'].user):
    #         return None
    #     _user = self.context['request'].user
    #     return obj.is_like(_user.id)

    def create(self, validated_data):
        ctx = self.context['request']
        post = BlogPost(created_user=ctx.user, **validated_data)
        if ctx.data.get('h_style') is not None:
            serializer = HeaderStyleSerializer(data=ctx.data['h_style'])
            if serializer.is_valid(raise_exception=True):
                post.h_style = HeaderStyle.objects.create(**serializer.validated_data)
        if ctx.data.get('text') is not None:
            text = ctx.data['text']
            if Limits.Post.min_text < len(text) < Limits.Post.max_text:
                post.text = text
        post.stext = html2text.html2text(post.text)
        post.link = make_text_link(post.header)
        post.save()
        return post


class BlogPostHtmlSerializer(serializers.ModelSerializer):
    created_user = UserMinSerializer(many=False, required=False)
    # is_like = serializers.SerializerMethodField('calc_is_liked')
    header = serializers.CharField(required=False, allow_blank=True, max_length=Limits.Post.max_header)
    text = serializers.CharField(max_length=Limits.Post.max_text, min_length=Limits.Post.min_text, required=False)
    link = serializers.CharField(max_length=Limits.Post.max_header, read_only=True, required=False)
    h_style = HeaderStyleSerializer(many=False, read_only=True, required=False)

    class Meta:
        model = BlogPost
        fields = ('id', 'header', 'text',  'created_user', 'created', 'h_style', 'link')


class SimplePostsSerializer(serializers.ModelSerializer):
    header = serializers.CharField(required=False, allow_blank=True, max_length=Limits.Post.max_header)
    h_style = HeaderStyleSerializer(many=False, read_only=True, required=False)
    text = serializers.CharField(max_length=Limits.Post.max_text, min_length=Limits.Post.min_text)
    helpers = serializers.IntegerField(read_only=True)
    is_like = serializers.SerializerMethodField('calc_is_liked')
    is_help = serializers.SerializerMethodField('calc_is_helped')
    distance = serializers.SerializerMethodField('calc_distance')
    show_header = serializers.SerializerMethodField('calc_show_header')
    created_user = UserMinSerializer(many=False, required=False)
    lhelpers = serializers.SerializerMethodField('calc_helpers')
    type = serializers.IntegerField(read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'header', 'text', 'is_like', 'is_help', 'created_user', 'distance',
                  'helpers', 'like', 'dislike', 'created', 'h_style', 'type', 'lhelpers',
                  'show_header', 'is_moderated', 'is_actually')

    def create(self, validated_data):
        post = self._create(validated_data)
        post.save()
        post = Qw.modify_posts_queryset(self.context['request'], [post])[0]
        return post

    def calc_helpers(self, obj):
        return [] if obj._helpers is None else obj._helpers

    def calc_show_header(self, obj):
        if is_anon(self.context['request'].user):
            return True
        if obj.h_style:
            return True
        return not self.context['request'].user.settings.only_custom_header

    def calc_is_liked(self, obj):
        return obj._is_liked

    def calc_is_helped(self, obj):
        return obj._is_help

    def calc_distance(self, obj):
        if is_anon(self.context['request'].user):
            return None
        _settings = self.context['request'].user.settings
        return obj.calc_distance(_settings)

    def _create(self, validated_data):
        ctx = self.context['request']
        post = Post(created_user=ctx.user, **validated_data)

        if ctx.data.get('condition') is not None:
            serializer = ConditionSerializer(data=ctx.data['condition'])
            if serializer.is_valid(raise_exception=True):
                post.condition, created = Condition.objects.get_or_create(**serializer.validated_data)
        if ctx.data.get('category') is not None:
            serializer = CategorySerializer(data=ctx.data['category'])
            if serializer.is_valid(raise_exception=True):
                post.category, created = Category.objects.get_or_create(**serializer.validated_data)
        if ctx.data.get('location') is not None:
            serializer = GeoInfoSerializer(data=ctx.data['location'])
            if serializer.is_valid(raise_exception=True):
                post.location = GeoInfo.objects.create(**serializer.validated_data)
        if ctx.data.get('h_style') is not None:
            serializer = HeaderStyleSerializer(data=ctx.data['h_style'])
            if serializer.is_valid(raise_exception=True):
                post.h_style = HeaderStyle.objects.create(**serializer.validated_data)

        ctx.user.inc_posts()
        return post


class EventPostsSerializer(SimplePostsSerializer):
    organization = serializers.CharField(max_length=Limits.Post.max_organization)
    req_helpers = serializers.IntegerField(min_value=1)

    class Meta(SimplePostsSerializer.Meta):
        fields = SimplePostsSerializer.Meta.fields + ('organization', 'req_helpers')

    def create(self, validated_data):
        post = self._create(validated_data)
        post.type = PostType.EVENT
        post.save()
        post = Qw.modify_posts_queryset(self.context['request'], [post])[0]
        return post


class PostDetailSerializer(SimplePostsSerializer):
    media = MediaUrlSerializer(many=True, read_only=True)
    condition = ConditionSerializer(many=False, read_only=True)
    category = CategorySerializer(many=False, read_only=True)
    created_user = UserMinSerializer(many=False, read_only=True)
    location = GeoInfoSerializer(many=False, required=False)

    class Meta(SimplePostsSerializer.Meta):
        fields = ('id', 'header', 'text', 'date_end', 'created_user', 'is_help', 'lhelpers',
                  'is_like', 'helpers', 'location', 'condition', 'category', 'views', 'media',
                  'is_actually', 'like', 'dislike', 'created', 'updated', 'h_style', 'type')


class EventDetailSerializer(PostDetailSerializer):
    class Meta(PostDetailSerializer.Meta):
        fields = PostDetailSerializer.Meta.fields + ('req_helpers', 'organization')


class CommentSerializerFields(serializers.ModelSerializer):
    created_user = UserMinSerializer(many=False, read_only=True)
    answered_him = UserMinSerializer(many=False, required=False, read_only=True)
    is_like = serializers.SerializerMethodField('calc_get_is_like')

    def calc_get_is_like(self, obj):
        if is_anon(self.context['request'].user):
            return None
        _user = self.context['request'].user
        return obj.is_like(_user.id)

    def _answered_him(self):
        try:
            return self.context['request'].data['answered_him']['id']
        except KeyError:
            return None


class PostCommentsSerializer(CommentSerializerFields):
    text = serializers.CharField(max_length=Limits.Comment.max_text)

    class Meta:
        model = CommentPost
        fields = ('id', 'created_user', 'answered_him', 'text',
                  'like', 'dislike', 'is_like', 'created')

    def create(self, validated_data):
        created = self.context['request'].user
        id_post = self.context['view'].kwargs['id_post']
        return CommentPost.objects.create(created_user=created, post_id=id_post,
                                          answered_him_id=self._answered_him(), **validated_data)


class UserCommentsSerializer(CommentSerializerFields):
    text = serializers.CharField(required=False, allow_blank=True, max_length=Limits.Comment.max_text)
    rating = serializers.IntegerField(required=True, min_value=Limits.Comment.min_rating,
                                      max_value=Limits.Comment.max_rating)

    class Meta:
        model = CommentUser
        fields = ('id', 'created_user', 'answered_him', 'text', 'rating',
                  'like', 'dislike', 'is_like', 'created')

    # todo: bad permission
    def create(self, validated_data):
        created = self.context['request'].user
        id_user = self.context['view'].kwargs['id_user']
        # UserCommentPermission.check_permission(self.context['request'], id_user)
        return CommentUser.objects.create(created_user=created, user_id=id_user,
                                          answered_him_id=self._answered_him(), **validated_data)


class HelpSerializer(serializers.HyperlinkedModelSerializer):
    user = UserMinSerializer(many=False, read_only=True)
    condition = ConditionSerializer(many=False, read_only=True)

    class Meta:
        model = Help
        fields = ('id', 'user', 'condition', 'created', 'status')


class RepliesSerializer(HelpSerializer):
    post = SimplePostsSerializer(many=False, read_only=True)
    public_contacts = serializers.SerializerMethodField('calc_public_contacts')

    def calc_public_contacts(self, obj):
        if obj.status == StatusType.SELECT_HELPER:
            user = User.objects.select_related('settings') \
                .only('contacts', 'settings__contacts') \
                .get(id=obj.post.created_user_id)

            public_contacts = user.public_contacts()
            return ContactsSerializer(public_contacts, many=True).data
        return []

    class Meta(HelpSerializer.Meta):
        fields = ('id', 'user', 'post', 'condition', 'created',
                  'public_contacts', 'status', 'updated')


class FeedbackSerializer(serializers.ModelSerializer):
    type = serializers.IntegerField(required=False)
    message = serializers.CharField(max_length=Limits.Feedback.max_msg,
                                    min_length=Limits.Feedback.min_msg,
                                    required=False, allow_blank=True)
    url = serializers.CharField(required=False, allow_blank=True,
                                min_length=Limits.Url.min_len_url,
                                max_length=Limits.Url.max_len_url)
    created_user = UserMinSerializer(many=False, required=False)
    rating = serializers.IntegerField(default=0, required=False)
    platform = serializers.CharField(max_length=Limits.Feedback.max_browser_os, required=False)
    email = serializers.EmailField(max_length=Limits.User.max_email, required=False)

    def create(self, validated_data):
        ctx = self.context['request']
        feedback = Feedback(created_user=ctx.user, **validated_data)
        feedback.save()
        return feedback

    class Meta:
        model = Feedback
        fields = ('type', 'message', 'created_user', 'url', 'rating',
                  'platform', 'email', 'created')