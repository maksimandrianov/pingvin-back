from django.db.models import Prefetch, F
from django.contrib.gis.measure import D, Distance
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.pagination import CursorPagination, _reverse_ordering
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from nmodels.models import User, UserSocialInfo, Post, Help, UserSettings, CommentUser, \
    CommentPost, Category, Feedback, MediaUrl, MediaType, BlogPost, Prompt
from nmodels.serializers import UsersSerializer, UserDetailSerializer, BlogPostSerializer, BlogPostHtmlSerializer
from nmodels.serializers import SimplePostsSerializer, PostDetailSerializer, EventPostsSerializer
from nmodels.serializers import HelpSerializer, RepliesSerializer, FeedbackSerializer, EventDetailSerializer
from nmodels.serializers import PostCommentsSerializer, UserCommentsSerializer, UserSettingsSerializer
from nmodels.permissions import SettingsPermission, NextPermission, IsOwnerOrReadOnlyPost, \
    IsNotSelfPost, PostBlogPermission, IsOwnerOrAdminOrReadOnlyPost, FeedbackPermission, IsOnlyAdmin, \
    IsOnlyAdminPermission
from nmodels.querysets import Qw
from nmodels.lib import is_anon, to_json, View
from djoser.views import RegistrationView, PasswordResetView
from django.http import Http404, JsonResponse
import hashlib, binascii


class CreateListViewSet(mixins.CreateModelMixin, mixins.ListModelMixin,
                        viewsets.GenericViewSet):
    pass


class ListViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    pass


class RetrieveUpdateViewSet(mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
                            viewsets.GenericViewSet):
    pass


class RetrieveViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    pass


class PostListDistanceCursorPagination(CursorPagination):
    ordering = ['distance', '-is_moderated', '-created']

    def paginate_queryset(self, queryset, request, view=None):
        self.page_size = self.get_page_size(request)
        if not self.page_size:
            return None

        self.base_url = request.build_absolute_uri()
        self.ordering = self.get_ordering(request, queryset, view)

        self.cursor = self.decode_cursor(request)
        if self.cursor is None:
            (offset, reverse, current_position) = (0, False, None)
        else:
            (offset, reverse, current_position) = self.cursor

        # Cursor pagination always enforces an ordering.
        if reverse:
            queryset = queryset.order_by(*_reverse_ordering(self.ordering))
        else:
            queryset = queryset.order_by(*self.ordering)

        print(self.ordering)

        # If we have a cursor with a fixed position then filter by that.
        if current_position is not None:
            order = self.ordering[0]
            is_reversed = order.startswith('-')
            order_attr = order.lstrip('-')

            # Test for: (cursor reversed) XOR (queryset reversed)
            if self.cursor.reverse != is_reversed:
                kwargs = {order_attr + '__lt': current_position}
            else:
                kwargs = {order_attr + '__gt': current_position}
            print(kwargs)
            r = current_position.split(' ')
            point = request.user.settings.location.point
            queryset = queryset.exclude(location__point__distance_lte=(point, D(meter=float(r[0]))))


        # If we have an offset cursor then offset the entire page by that amount.
        # We also always fetch an extra item in order to determine if there is a
        # page following on from this one.
        results = list(queryset[offset:offset + self.page_size + 1])
        self.page = list(results[:self.page_size])

        # Determine the position of the final item following the page.
        if len(results) > len(self.page):
            has_following_postion = True
            following_position = self._get_position_from_instance(results[-1], self.ordering)
        else:
            has_following_postion = False
            following_position = None

        # If we have a reverse queryset, then the query ordering was in reverse
        # so we need to reverse the items again before returning them to the user.
        if reverse:
            self.page = list(reversed(self.page))

        if reverse:
            # Determine next and previous positions for reverse cursors.
            self.has_next = (current_position is not None) or (offset > 0)
            self.has_previous = has_following_postion
            if self.has_next:
                self.next_position = current_position
            if self.has_previous:
                self.previous_position = following_position
        else:
            # Determine next and previous positions for forward cursors.
            self.has_next = has_following_postion
            self.has_previous = (current_position is not None) or (offset > 0)
            if self.has_next:
                self.next_position = following_position
            if self.has_previous:
                self.previous_position = current_position

        # Display page controls in the browsable API if there is more
        # than one page.
        if (self.has_previous or self.has_next) and self.template is not None:
            self.display_page_controls = True

        return self.page


class UpdateCursorPagination(CursorPagination):
    ordering = 'updated'


class MinusUpdateCursorPagination(CursorPagination):
    ordering = '-updated'


class UserRatingCursorPagination(CursorPagination):
    ordering = ['-rating', '-created']


class PostListMixin:
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            page = Qw.modify_posts_queryset(request, page)
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        queryset = Qw.modify_posts_queryset(request, queryset)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class UsersList(ListViewSet):
    queryset = User.activated() \
        .prefetch_related(Prefetch('categories', queryset=Category
                                   .objects.all().only('category_type', 'subcategory_type'))) \
        .defer('password', 'last_login', 'is_superuser', 'updated',
               'email', 'info', 'activity', 'is_admin', 'bday', 'is_active') \
        .annotate(rating=F('helps') * 0.5 + F('mhelps') * 0.3)
    serializer_class = UsersSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, NextPermission,)
    pagination_class = UserRatingCursorPagination

    @list_route(methods=['GET'], permission_classes=[IsAuthenticated], url_path='search')
    def search(self, request):
        user = request.user
        params = request.query_params
        queryset = User.search(params) \
            .annotate(rating=F('helps') * 0.5 + F('mhelps') * 0.3)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class SimplePostsList(PostListMixin, CreateListViewSet):
    queryset = Qw.posts_list_optimization(Post.simple_posts())
    serializer_class = SimplePostsSerializer
    pagination_class = PostListDistanceCursorPagination
    # permission (NextPermission)
    permission_classes = (IsAuthenticatedOrReadOnly, NextPermission,)

    def get_queryset(self):
        if is_anon(self.request.user):
            self.pagination_class = CursorPagination
            return self.queryset

        settings = self.request.user.settings
        queryset = settings.apply_settings_category(self.queryset)
        if not self.request.user.settings.is_set_location():
            self.pagination_class = CursorPagination
            return queryset

        point = settings.location.point
        return queryset.filter(location__isnull=False).distance(point, field_name='location__point')

    @list_route(methods=['GET'], permission_classes=[IsAuthenticated], url_path='search')
    def search(self, request):
        user = request.user
        params = request.query_params
        queryset = Qw.posts_list_optimization(Post.search(params))
        self.pagination_class = CursorPagination
        page = self.paginate_queryset(queryset)
        if page is not None:
            page = Qw.modify_posts_queryset(request, page)
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        queryset = Qw.modify_posts_queryset(request, queryset)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class BlockedPostsList(PostListMixin, ListViewSet):
    queryset = Post.blocked_posts() \
        .prefetch_related(Prefetch('created_user', queryset=Qw.queryset_user_min)) \
        .defer('updated', 'category_id', 'condition_id',
               'req_helpers', 'moderated_time', 'organization')
    serializer_class = SimplePostsSerializer
    permission_classes = (IsOnlyAdminPermission,)


class ModeratedPostsList(PostListMixin, ListViewSet):
    queryset = Post.non_moderated_posts() \
        .prefetch_related(Prefetch('created_user', queryset=Qw.queryset_user_min)) \
        .defer('updated', 'category_id', 'condition_id',
               'req_helpers', 'moderated_time', 'organization')
    serializer_class = SimplePostsSerializer
    permission_classes = (IsOnlyAdminPermission,)


class BlogPostsList(CreateListViewSet):
    queryset = BlogPost.objects.all() \
        .select_related('created_user') \
        .select_related('h_style') \
        .select_related('created_user__avatar') \
        .only('header', 'stext', 'created', 'h_style__img',
              'h_style__img_orig', 'h_style__b_color', 'created_user__first_name',
              'created_user__second_name', 'created_user__avatar__url',
              'created_user__avatar__type')
    serializer_class = BlogPostSerializer
    permission_classes = (PostBlogPermission,)


class BlogPostDetail(mixins.DestroyModelMixin, RetrieveViewSet):
    lookup_field = 'link'
    queryset = BlogPost.objects.all() \
        .select_related('created_user') \
        .select_related('h_style') \
        .select_related('created_user__avatar') \
        .only('header', 'text', 'created', 'h_style__img',
              'h_style__img_orig', 'h_style__b_color', 'created_user__first_name',
              'created_user__second_name', 'created_user__avatar__url',
              'created_user__avatar__type')
    serializer_class = BlogPostHtmlSerializer
    permission_classes = (PostBlogPermission,)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class FeedbackList(CreateListViewSet):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    permission_classes = (FeedbackPermission,)


class EventPostsList(SimplePostsList):
    queryset = Qw.events_list_optimization(Post.event_posts())
    serializer_class = EventPostsSerializer


class SettingsDetail(RetrieveUpdateViewSet):
    queryset = UserSettings.objects.all()
    serializer_class = UserSettingsSerializer
    permission_classes = (SettingsPermission,)


class PostCommentList(CreateListViewSet):
    serializer_class = PostCommentsSerializer
    queryset = CommentPost.objects.all() \
        .prefetch_related(Prefetch('created_user', queryset=Qw.queryset_user_min),
                          Prefetch('answered_him', queryset=Qw.queryset_user_min)) \
        .defer('updated')
    permission_classes = (IsAuthenticatedOrReadOnly, NextPermission,)

    def get_queryset(self):
        post_id = self.kwargs['id_post']
        return self.queryset.filter(post_id=post_id)


class UserCommentList(CreateListViewSet):
    serializer_class = UserCommentsSerializer
    queryset = CommentUser.objects.all() \
        .prefetch_related(Prefetch('created_user', queryset=Qw.queryset_user_min),
                          Prefetch('answered_him', queryset=Qw.queryset_user_min),
                          Prefetch('user', queryset=User.objects.all().only('id'))) \
        .defer('updated')
    # todo: not push if user not help
    permission_classes = (IsAuthenticatedOrReadOnly, NextPermission,)

    def get_queryset(self):
        user_id = self.kwargs['id_user']
        return self.queryset.filter(user_id=user_id)


class RepliesList(ListViewSet):
    queryset = Help.objects.none()
    serializer_class = RepliesSerializer
    pagination_class = MinusUpdateCursorPagination
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):

        user = self.request.user
        user.see_replies()
        return user.replies() \
            .select_related('condition') \
            .select_related('post') \
            .prefetch_related(Prefetch('user', queryset=Qw.queryset_user_min)) \
            .prefetch_related(Prefetch('post__created_user', queryset=Qw.queryset_user_min)) \
            .select_related('post__h_style') \
            .select_related('post__location') \
            .defer('post__updated', 'post__category_id', 'post__condition_id',
                   'post__location__city', 'post__location__country', 'post__location__address',
                   'post__views', 'post__is_blocked', 'post__media', 'post__req_helpers',
                   'post__moderated_time', 'post__organization')

    def modify_replies(self,request, replies):
        posts = [r.post for r in replies]
        posts = Qw.modify_posts_queryset(request, posts)
        for i in range(len(replies)):
            replies[i].post = posts[i]
        return replies

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            page = self.modify_replies(request, page)
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        queryset = self.modify_replies(request, queryset)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['GET'], url_path='count')
    def count_replies(self, request):
        user = request.user
        count = user.count_new_replies()
        return Response({'count': count})


class MyPostsList(PostListMixin, ListViewSet):
    queryset = Post.objects.none()
    serializer_class = SimplePostsSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        return Qw.posts_list_optimization(user.my_posts())


class MyHelpPostList(MyPostsList):

    def get_queryset(self):
        user = self.request.user
        return Qw.posts_list_optimization(user.my_help_posts())


class HelpList(ListViewSet):
    serializer_class = HelpSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        post_id = self.kwargs['id_post']
        return Help.objects.filter(post_id=post_id, is_head=True)


class SimplePostDetail(RetrieveViewSet):
    queryset = Post.simple_posts(is_actually=False) \
        .prefetch_related(Prefetch('created_user', queryset=Qw.queryset_user_min)) \
        .select_related('category') \
        .select_related('condition') \
        .select_related('h_style') \
        .select_related('location')
    serializer_class = PostDetailSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        instance = Qw.modify_posts_queryset(request, [instance])[0]
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(created_user=self.request.user)

    def get_queryset(self):
        if not is_anon(self.request.user) and self.request.user.is_admin:
            return Post.all_simple_posts()
        return self.queryset

    def get_object(self):
        post = super(self.__class__, self).get_object()
        user = self.request.user
        if not user.is_anonymous():
            post.inc_views()
        return post


class EventPostDetail(SimplePostDetail):
    serializer_class = EventDetailSerializer
    queryset = Post.objects.none()

    def get_queryset(self):
        pk = int(self.kwargs['pk'])
        user = self.request.user
        posts = Post.event(pk)
        post = posts[0]
        if not (post.is_moderated or (not is_anon(user) and (post.created_user.pk == user.pk or user.is_admin))):
            raise Http404()
        if not user.is_anonymous():
            posts[0].inc_views()
        return posts \
            .prefetch_related(Prefetch('created_user', queryset=Qw.queryset_user_min)) \
            .select_related('category') \
            .select_related('condition') \
            .select_related('h_style') \
            .select_related('location')

    def get_object(self):
        return super(SimplePostDetail, self).get_object()


class UserDetail(RetrieveUpdateViewSet):
    queryset = User.activated()
    serializer_class = UserDetailSerializer
    # permission (default)

    # permission (IsNotSelfPost)
    @detail_route(methods=['POST'], url_path='help')
    def help(self, request, pk=None):
        post_id = request.data['post']
        condition_dict = request.data['condition']
        user = request.user
        IsNotSelfPost.check_permission(request, post_id)
        _help = user.help(post_id, condition_dict)
        return Response(HelpSerializer(_help, many=False).data)

    # permission (IsNotSelfPost)
    @detail_route(methods=['POST'], url_path='cancel-help')
    def cancel_help(self, request, pk=None):
        post_id = request.data['post']
        user = request.user
        IsNotSelfPost.check_permission(request, post_id)
        user.cancel_help(post_id)
        return Response()

    # permission (default)
    @detail_route(methods=['POST'], url_path='update-condition-help')
    def update_condition_help(self, request, pk=None):
        post_id = request.data['post']
        condition_dict = request.data['condition']
        user = request.user
        _help = user.update_condition_help(post_id, condition_dict)
        return Response(HelpSerializer(_help, many=False).data)

    # permission (IsOwnerOrReadOnlyPost)
    @detail_route(methods=['POST'])
    def close(self, request, pk=None):
        post_id = request.data['post']
        user = request.user
        IsOwnerOrAdminOrReadOnlyPost.check_permission(request, post_id)
        post = user.close(post_id)
        post = Qw.modify_posts_queryset(request, [post])[0]
        serializer = PostDetailSerializer(post, many=False, context={'request': request})
        return Response(serializer.data)

    @detail_route(methods=['POST'])
    def unblock(self, request, pk=None):
        post_id = request.data['post']
        user = request.user
        IsOnlyAdmin.check_permission(request)
        post = user.unblock(post_id)
        post = Qw.modify_posts_queryset(request, [post])[0]
        serializer = PostDetailSerializer(post, many=False, context={'request': request})
        return Response(serializer.data)

    @detail_route(methods=['POST'], url_path='moderation-over')
    def moderation_over(self, request, pk=None):
        post_id = request.data['post']
        user = request.user
        IsOnlyAdmin.check_permission(request)
        post = user.moderation_over(post_id)
        post = Qw.modify_posts_queryset(request, [post])[0]
        serializer = PostDetailSerializer(post, many=False, context={'request': request})
        return Response(serializer.data)

    # permission (IsOwnerOrReadOnlyPost)
    @detail_route(methods=['POST'], url_path='select-helper')
    def select_helper(self, request, pk=None):
        post_id = request.data['post']
        user_id = request.data['user']
        user = request.user
        IsOwnerOrReadOnlyPost.check_permission(request, post_id)
        help = user.select_helper(post_id, user_id)
        return Response(HelpSerializer(help, many=False).data)

    # permission (IsOwnerOrReadOnlyPost)
    @detail_route(methods=['POST'], url_path='cancel-helper')
    def cancel_helper(self, request, pk=None):
        post_id = request.data['post']
        user_id = request.data['user']
        user = request.user
        IsOwnerOrReadOnlyPost.check_permission(request, post_id)
        help = user.cancel_helper(post_id, user_id)
        return Response(HelpSerializer(help, many=False).data)

    # permission (IsOwnerOrReadOnlyPost)
    @detail_route(methods=['POST'], url_path='select-successfully')
    def select_successfully(self, request, pk=None):
        post_id = request.data['post']
        user_id = request.data['user']
        user = request.user
        IsOwnerOrReadOnlyPost.check_permission(request, post_id)
        help = user.select_successfully(post_id, user_id)
        return Response(HelpSerializer(help, many=False).data)

    # test transaction +
    # test required field +
    # test required permission +
    # permission (IsOwnerOrReadOnlyPost)
    @detail_route(methods=['POST'], url_path='cselect-successfully')
    def cselect_successfully(self, request, pk=None):
        post_id = request.data['post']
        user_id = request.data['user']
        comment = request.data['comment']
        user = request.user
        IsOwnerOrReadOnlyPost.check_permission(request, post_id)
        help = user.select_successfully(post_id, user_id)

        ser_comment = UserCommentsSerializer(data=comment, context={
            'request': request,
            'view': View({'id_user': user_id})
        })

        if ser_comment.is_valid(raise_exception=True):
            ser_comment.save()
        return Response(HelpSerializer(help, many=False).data)

    # permission (default)
    @detail_route(methods=['POST'], url_path='like-post')
    def like_post(self, request, pk=None):
        obj_id = request.data['post']
        user = request.user
        like = user.like_post(obj_id)
        return Response({'like': like.is_like})

    # permission (default)
    @detail_route(methods=['POST'], url_path='dislike-post')
    def dislike_post(self, request, pk=None):
        obj_id = request.data['post']
        user = request.user
        like = user.dislike_post(obj_id)
        return Response({'like': like.is_like})

    # permission (default)
    @detail_route(methods=['POST'], url_path='like-comment-post')
    def like_comment_post(self, request, pk=None):
        obj_id = request.data['comment']
        user = request.user
        like = user.like_comment_post(obj_id)
        return Response({'like': like.is_like})

    # permission (default)
    @detail_route(methods=['POST'], url_path='dislike-comment-post')
    def dislike_comment_post(self, request, pk=None):
        obj_id = request.data['comment']
        user = request.user
        like = user.dislike_comment_post(obj_id)
        return Response({'like': like.is_like})

    # permission (default)
    @detail_route(methods=['POST'], url_path='like-comment-user')
    def like_comment_user(self, request, pk=None):
        obj_id = request.data['comment']
        user = request.user
        like = user.like_comment_user(obj_id)
        return Response({'like': like.is_like})

    # permission (default)
    @detail_route(methods=['POST'], url_path='dislike-comment-user')
    def dislike_comment_user(self, request, pk=None):
        obj_id = request.data['comment']
        user = request.user
        like = user.dislike_comment_user(obj_id)
        return Response({'like': like.is_like})

    # permission (default)
    @detail_route(methods=['POST'], url_path='like-blog-post')
    def like_blog_post(self, request, pk=None):
        obj_id = request.data['blog_post']
        user = request.user
        like = user.like_blog_post(obj_id)
        return Response({'like': like.is_like})

    # permission (default)
    @detail_route(methods=['POST'], url_path='dislike-blog-post')
    def dislike_blog_post(self, request, pk=None):
        obj_id = request.data['blog_post']
        user = request.user
        like = user.dislike_blog_post(obj_id)
        return Response({'like': like.is_like})

    # permission (default)
    @detail_route(methods=['POST'], url_path='prompt-feedback')
    def prompt_feedback(self, request, pk=None):
        user = request.user
        prompt = Prompt.get_prompt(user)
        result = prompt.prompt_feedback()
        return Response(result)

    # permission (default)
    @detail_route(methods=['POST'], url_path='prompt-publications')
    def prompt_publications(self, request, pk=None):
        user = request.user
        prompt = Prompt.get_prompt(user)
        result = prompt.prompt_publications()
        return Response(result)

    # permission (default)
    @detail_route(methods=['POST'], url_path='prompt-helps')
    def prompt_helps(self, request, pk=None):
        user = request.user
        prompt = Prompt.get_prompt(user)
        result = prompt.prompt_helps()
        return Response(result)

    # permission (default)
    @detail_route(methods=['POST'], url_path='close-prompt-feedback')
    def close_prompt_feedback(self, request, pk=None):
        user = request.user
        prompt = Prompt.get_prompt(user)
        result = prompt.defer_prompt_feedback()
        return Response(result)

    # permission (default)
    @detail_route(methods=['POST'], url_path='close-prompt-publications')
    def close_prompt_publications(self, request, pk=None):
        user = request.user
        prompt = Prompt.get_prompt(user)
        result = prompt.defer_prompt_publications()
        return Response(result)

    # permission (default)
    @detail_route(methods=['POST'], url_path='close-prompt-helps')
    def close_prompt_helps(self, request, pk=None):
        user = request.user
        prompt = Prompt.get_prompt(user)
        result = prompt.defer_prompt_helps()
        return Response(result)

    # permission (default)
    @detail_route(methods=['POST'], url_path='skip-prompt-feedback')
    def skip_prompt_feedback(self, request, pk=None):
        user = request.user
        prompt = Prompt.get_prompt(user)
        result = prompt.skip_prompt_feedback()
        return Response(result)

    # permission (default)
    @detail_route(methods=['POST'], url_path='skip-prompt-publications')
    def skip_prompt_publications(self, request, pk=None):
        user = request.user
        prompt = Prompt.get_prompt(user)
        result = prompt.skip_prompt_publications()
        return Response(result)

    # permission (default)
    @detail_route(methods=['POST'], url_path='skip-prompt-helps')
    def skip_prompt_helps(self, request, pk=None):
        user = request.user
        prompt = Prompt.get_prompt(user)
        result = prompt.skip_prompt_helps()
        return Response(result)


class CustomRegistrationView(RegistrationView):
    html_body_template_name = 'activation_email_body.html'


class CustomPasswordResetView(PasswordResetView):
    html_body_template_name = 'password_reset_email_body.html'


def is_exists_social(sn_type, user_id):
    try:
        return UserSocialInfo.objects.get(sn_type=sn_type, user_id=user_id)
    except UserSocialInfo.DoesNotExist:
        return False


def gen_pswd(email):
    SALT = b'c7F65827&,jfs!'
    dk = hashlib.pbkdf2_hmac('sha256',str.encode(email), SALT, 2048)
    return str(binascii.hexlify(dk))


def social_auth(request):
    if request.method != 'POST':
        return JsonResponse({'status': -1})
    u = to_json(request.body)
    user_id = str(u['user_id'])
    if is_exists_social(u['sn'], user_id):
        return JsonResponse({'status': 1})
    social = UserSocialInfo(sn_type=u['sn'], user_id=user_id, email=u['email'])
    social.save()
    avatar = None
    if u['avatar']['url']:
        avatar = MediaUrl(url=u['avatar']['url'], type=MediaType.SOCIAL_AVATAR)
        avatar.save()
    user = User.objects.create_user(u['fake_email'], gen_pswd(u['fake_email']),
                                    u['first_name'], u['second_name'])
    user.is_active =True
    user.gender = u['gender']
    user.bday = u['bday']
    user.avatar = avatar
    user.save()
    return JsonResponse({'status': 2})