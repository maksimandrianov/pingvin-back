from django.db.models import Q
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from nmodels.lib import is_anon
from nmodels.models import Post, Help, StatusType
import logging


class SettingsPermission(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return obj.pk == request.user.pk


class IsOwnerOrReadOnlyPost:

    @staticmethod
    def check_permission(request, post_id, view=None, obj=None):
        ex = PermissionDenied('is owner or read only post permission error')
        try:
            post = Post.objects.get(id=post_id)
        except Post.DoesNotExist:
            raise ex
        if post.created_user.id != request.user.id:
            raise ex
        return True


class IsOnlyAdmin:

    @staticmethod
    def check_permission(request, view=None, obj=None):
        if request.user.is_admin:
            return True
        raise PermissionDenied('only admin')



class IsOnlyAdminPermission(permissions.BasePermission):

    def has_permission(self, request, view):
            return request.user.is_admin


class IsOwnerOrAdminOrReadOnlyPost:

    @staticmethod
    def check_permission(request, post_id, view=None, obj=None):
        if not is_anon(request.user) and request.user.is_admin:
            return True
        return IsOwnerOrReadOnlyPost.check_permission(request, post_id)


class FeedbackPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method == 'GET':
            return not is_anon(request.user) and request.user.is_admin
        else:
            return request.user


class PostBlogPermission(IsAuthenticatedOrReadOnly):

    def has_permission(self, request, view):
        if request.method == 'POST' or request.method == 'DELETE':
            return request.user.is_admin
        return super(IsAuthenticatedOrReadOnly, self).has_permission(request, view)


class IsNotSelfPost:

    @staticmethod
    def check_permission(request, post_id, view=None, obj=None):
        ex = PermissionDenied('is not self post permission error')
        try:
            post = Post.actually_q().get(id=post_id)
        except Post.DoesNotExist:
            raise ex
        if post.created_user.id == request.user.id:
            raise ex
        return True


class NextPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if is_anon(request.user) and request.method == 'GET' and 'cursor' in request.GET:
            logging.warning('Next permission error, is anon - ' + str(is_anon(request.user)))
            raise PermissionDenied('Next permission error')

        return True


class UserCommentPermission:

    @staticmethod
    def check_permission(request, user_id, view=None, obj=None):
        if request.method == 'POST':
            ex = PermissionDenied('User comment permission error')
            try:
                help = Help.objects.get(Q(user_id=request.user.id) & Q(post__created_user_id=user_id))
            except Help.DoesNotExist:
                raise ex
            if help.status != StatusType.FINIS_HELP:
                raise ex
        return True