#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nproject.settings")
    from django.core.management import execute_from_command_line

    if 'test' in sys.argv:
        PASSWORD_HASHERS = ('django_plainpasswordhasher.PlainPasswordHasher', )
        DEFAULT_FILE_STORAGE = 'inmemorystorage.InMemoryStorage'

    execute_from_command_line(sys.argv)

