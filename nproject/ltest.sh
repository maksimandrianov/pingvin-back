#!/bin/bash

cd "$pwd"
source ../bin/activate 
locust -f nmodels/tests/locustfile.py --host=http://127.0.0.1:8800 --print-stats --only-summary --no-web -c 8 -r 1000 -n 5000

